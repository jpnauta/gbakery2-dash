var _ = require('lodash');
var argv = require('yargs').argv;

var defaults = {
    reporters: ['progress'],  // Test reporter
    browsers: ['PhantomJS'],
    dist: false
};

var args = {
    reporters: argv.reporters && argv.reporters.split(',') || ['progress'],  // Test reporter
    browsers: argv.browsers && argv.browsers.split(',') || ['PhantomJS'],  // Browsers to run on
    dist: argv.dist  // If true, run tests dist files instead of src
};

var config = _.assign({}, defaults, args);

module.exports = config;