var sharedConfig = require('../karma.shared.conf');

var testConfig = require('../config');

module.exports = function (config) {
    // Apply shared config
    sharedConfig(config);

    config.set({
        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: [
            '../../dist/core.vendor.js',
            '../../libs/angular-mocks/angular-mocks.js',
            '../../dist/core.js',
            '**/*-specs.js'
        ],

        // list of files to exclude
        exclude: [],

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {},

        // web server port
        port: 9877
    });
};
