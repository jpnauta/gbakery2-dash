# gbakery2-dash

[![CircleCI](https://circleci.com/bb/jpnauta/gbakery2-dash.svg?style=svg)](https://circleci.com/bb/jpnauta/gbakery2-dash)

*Dashboard application for Glamorgan Bakery system.*

## Getting Started

### Pre-requisites
This project uses [docker-compose](https://docs.docker.com/compose/) for development. 
Please set up your machine to to run the `docker-compose` command before proceeding.

### Configuration

You will need to set up your environment file. To do this, simply copy the default
constants file into your directory folder with this command:

```
cp TEMPLATE.env .env
```

### Start Server
You can now start your server for the first time! To do this run this command:

```
docker-compose build
docker-compose up
```

*Note: It will take several minutes to build your environment for the first time* 

## Development

### Running
To start developing this project, simply start the project with `docker-compose`.

```
docker-compose build
docker-compose up
```

## Knowledge Base

All passwords, keys, account details, etc. are stored on Glamorgan Bakery's 
[Slack account](https://glamorganbakery.slack.com/).


## Continuous Integration

To simplify deployment, continuous integration has been configured using [Circle CI][circleci]. Any
changes pushed to `master` branch will be automatically deployed to the live server.

[circleci]: https://circleci.com/bb/jpnauta/gbakery2-dash
