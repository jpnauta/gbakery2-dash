const env = require('env-var');

// Load .env file into process environment variables
require('dotenv').config();

const APP_URL = env.get('APP_URL').required().asString();

const MINIFY = env.get('MINIFY', 'false').asBool();

const DEPRECATION_COMPLETE_LEVEL = env.get('DEPRECATION_COMPLETE_LEVEL', 'WARNING').asString();
const DEPRECATION_PENDING_LEVEL = env.get('DEPRECATION_COMPLETE_LEVEL', 'IGNORE').asString();

const settings = {
  deprecation: {
    complete: {
      level: DEPRECATION_COMPLETE_LEVEL
    },
    pending: {
      level: DEPRECATION_PENDING_LEVEL
    }
  },
  app: {
    url: APP_URL,
  },
};

const config = {
  minify: MINIFY,  // If true, builds will minify static assets
  settings: settings,  // Constants injected into javascript. See `core.ext.config` for options
};

module.exports = config;
