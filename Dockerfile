FROM node:14 as build

ENV APP_URL 'https://app.glamorganbakery.com'

RUN apt-get -y update && \
    # Git
    apt-get -y install git && \
    # bash
    apt-get -y install bash

RUN curl -o- -L https://yarnpkg.com/install.sh | bash

WORKDIR /static/

# Build static requirements
ADD package.json .
ADD yarn.lock .
RUN yarn install

ADD src/ src/
ADD config.js .
ADD webpack.config.js .

RUN npm run build:prod

FROM node:14 as runtime

RUN npm install -q -g superstatic

WORKDIR /static/

COPY --from=build /static/dist/ /static/dist/
ADD superstatic.json .
ADD package.json .

CMD npm run start:prod