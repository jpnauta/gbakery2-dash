import { defaultsDeep } from 'lodash';

const defaults = {
  deprecation: {
    pending: {
      level: 'WARNING'  // Log level for pending deprecations
    },
    complete: {
      level: 'ERROR'  // Log level for completed deprecations
    }
  }
};

export const CONFIG = defaultsDeep(process.env.CONFIG, defaults);
