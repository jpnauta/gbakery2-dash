import { productQuantity } from "./filters/product_quantity";
import { BakerProduct } from "./models/baker_product";
import { FrontProduct } from "./models/front_product";
import { FrontProductNotReady, SyncDashFrontProductNotReady } from "./models/front_product_not_ready";
import { ProductCategory } from "./models/product_category";

export const DashProductsModule = angular.module('dash.products', [])
    .filter('productQuantity', productQuantity)
    .factory('BakerProduct', BakerProduct)
    .factory('FrontProduct', FrontProduct)
    .factory('FrontProductNotReady', FrontProductNotReady)
    .factory('SyncDashFrontProductNotReady', SyncDashFrontProductNotReady)
    .factory('ProductCategory', ProductCategory);