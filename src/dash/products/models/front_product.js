/* @ngInject */
export function FrontProduct(ModelFactory, _) {
  let FrontProduct = ModelFactory({
    baseUrl: '/api/dashboard/products/',
    pkField: 'slug',
    paginated: false,
    actions: {
      stats: {
        method: 'GET',
        params: {
          listCtrl: 'stats'
        },
        isArray: true
      }
    }
  });

  FrontProduct.all = function (params) {
    return FrontProduct.query(params).$promise.then(function (products) {
      _.each(products, function (product) {
        product.rating = Number(product.rating);
      });

      return products;
    });
  };

  return FrontProduct;
}