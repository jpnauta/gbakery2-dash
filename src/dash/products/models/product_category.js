/* @ngInject */
export function ProductCategory(ModelFactory) {
  return ModelFactory({
    baseUrl: '/api/dashboard/products/categories/',
    pkField: 'slug',
    paginated: false,
    actions: {
      report: {
        method: 'GET',
        params: {
          listCtrl: 'report'
        },
        isArray: true
      }
    }
  });
}