/* @ngInject */
export function BakerProduct(ModelFactory) {
  return ModelFactory({
    baseUrl: '/api/dashboard/baker-products/',
    pkField: 'slug',
    paginated: false
  });
}