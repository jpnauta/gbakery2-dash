/* @ngInject */
export function FrontProductNotReady(ModelFactory, $utils, _) {
  var FrontProductNotReady = ModelFactory({
    baseUrl: '/api/dashboard/products/not-ready/',
    paginated: false
  });

  //Override save function to format date correctly during save
  var orig = FrontProductNotReady.prototype.save;
  FrontProductNotReady.prototype.save = function () {
    var params = _.clone(this);
    params.date = $utils.dateToJson(this.date);

    return orig.apply(new FrontProductNotReady(params), arguments);
  };

  FrontProductNotReady.all = function (opts) {
    opts = _.assign({
      details: true //Tells the server to load the actual front product of order front products
    }, opts);

    return FrontProductNotReady.query(opts).$promise;
  };

  return FrontProductNotReady;
}

/* @ngInject */
export function SyncDashFrontProductNotReady(SyncModelFactory) {
  return SyncModelFactory({
    syncUrl: 'dashboard/products/products/not-ready',
    baseUrl: '/api/sync/dashboard/products/products/not-ready/',
    paginated: false
  });
}