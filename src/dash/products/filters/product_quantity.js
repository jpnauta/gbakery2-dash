/* @ngInject */
export function productQuantity() {
  return function (total) {
    //Display whole number if given, otherwise round to 2 decimal places
    if (total % 1 == 0) {
      return total;
    } else {
      return total.toFixed(2);
    }
  };
}
