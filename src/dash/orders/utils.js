import {CoreUsersSessionsModule} from "../../core/users/sessions";
import {renderTemplate} from "../../core/generic/templates";

export const DashOrdersUtilsModule = angular.module('dash.orders.utils', [CoreUsersSessionsModule.name])
    .factory('$orderUtils', function ($utils, $constants, $timeout, LoaderFactory, Delayer, Order, FrontProduct,
                                      $filter, $q, $notifier, $mdDialog, ipCookie, OrderFlag, FrontProductNotReady,
                                      OrderLocation, _, $, storage, moment, ModelBridge, auth) {
        var ALTER_ORDER_STORAGE_KEY = 'order-created';

        const confirmVoidOrRefundDialog = (title, message, order, ev) => {
            const confirmVoidOrRefund = $mdDialog.confirm()
                .parent(angular.element(document.body))
                .title(title)
                .content(message)
                .ok('Yes')
                .cancel('No')
                .targetEvent(ev);

            return $mdDialog.show(confirmVoidOrRefund);
        }

        var $orderUtils = {
            newEditUtils: function ($scope) {
                $scope.removeOrderProduct = function (ofp) {
                    $scope.order.order_products.remove(ofp);
                };

                $scope.addOrderFrontProduct = function (op = {product_type: "front_product"}) {
                    var order = $scope.order;
                    order.order_products = order.order_products || [];
                    order.order_products.push(op);
                };

                $scope.addSpecialProduct = function (op = {product_type: 'special_product'}) {
                    return $scope.addOrderFrontProduct(op);
                };

                $scope.ofpHasUserInput = function (ofp) {
                    return ofp.name || ofp.quantity_type || ofp.quantity;
                };

                $scope.orderFrontProductRequired = function (ofp) {
                    //Only require ofp fields if it's not empty
                    return $scope.ofpHasUserInput(ofp);
                };

                $scope.getProductName = function (id) {
                    return _.find($scope.products, {id: id}).name;
                };

                $scope.getProductIsStarred = function (id) {
                    return _.find($scope.products, {id: id}).is_starred;
                };

                $scope.getTimeStr = function (str) {
                    return _.find($scope.$timeOptions, {str: str}).str12hMed;
                };

                $scope.ofpFrontProductChanged = function (ofp) {
                    var product = _.find($scope.products, {id: ofp.front_product});

                    if (product) {
                        ofp.quantity_type = product.quantity_type;
                    }

                    var ops = $scope.order && $scope.order.order_products,
                        last = _.last(ops);

                    if (!last || $scope.ofpHasUserInput(last)) {
                        $scope.addOrderFrontProduct();
                    }
                };

                $scope.toggleMarkedAsPaid = function (order) {
                    if (!order.stripe_charge_id) {
                        order.marked_as_paid = !order.marked_as_paid
                    }
                };

                $scope.orderIsPaidOrPendingPayment = function (order) {
                    return order.marked_as_paid || order.purchase_status == "paid" || order.purchase_status == "payment_pending";
                }

                $scope.$cleanOrder = function (oldOrder) {
                    var order = new Order(oldOrder);

                    //Remove empty order products before save
                    order.order_products = _.filter(order.order_products, function (ofp) {
                        return $scope.ofpHasUserInput(ofp);
                    });

                    return order;
                };

                $scope.$saveOrder = function () {
                    //Create clone of order to allow for minor modifications before save
                    var order = $scope.$cleanOrder($scope.order);

                    //Save order
                    return order.save();
                };
            },
            newEditCommon: function ($scope) {
                var load = LoaderFactory.errorOnly();

                $orderUtils.newEditUtils($scope);

                $scope.$quantityTypes = $constants.QUANTITY_TYPE_LIST;
                $scope.$timeOptions = $utils.createStdTimeSets();

                $scope.params = {};

                $scope.$printOrderAfterSave = false;
                $scope.$sendOrResendStripeInvoice = false;
                $scope.$markInvoiceAsPaid = false;

                $scope.$watch('order.custom_price', () => {
                    if ($scope.order.custom_price) {
                        $scope.order.custom_price = Number($scope.order.custom_price);
                    }
                });

                $scope.$loadOrderIfSpecified = function () {
                    var id = $scope.$$id;
                    if (angular.isDefined(id)) {
                        $scope.$$id = null; //Clear the ID for next time
                        return $scope.$loadOrder(id);
                    }
                    return $q.when(); //Return empty promise
                };

                $scope.$loadOrder = function (id) {
                    return load(Order.get({id: id}).$promise.then(function (order) {
                        //Set special flag to indicate that ops were loaded from server - fixes ui-select ng-change glitch
                        _.each(order.order_products, function (op) {
                            op.$$init = true;
                        });

                        $scope.order = order;

                        // Convert string to Date, if needed
                        if ($scope.order.date) {
                            $scope.order.date = moment($scope.order.date).toDate();
                        }
                    }));
                };

                $scope.$loadFrontProducts = function () {
                    return FrontProduct.all().then(function (products) {
                        $scope.products = products;
                    });
                };

                $scope.$loadOrderFlags = function () {
                    return OrderFlag.query().$promise.then(function (orderFlags) {
                        $scope.orderFlags = orderFlags;
                    });
                };

                $scope.$loadAll = function () {
                    var promise = $q.all([
                        $scope.$loadFrontProducts(),
                        $scope.$loadOrderFlags()
                    ])
                        .then(function () {
                            return $scope.$loadOrderIfSpecified().then(function () {
                                $scope.focusFirst = true; //Focus on first field at start
                                $scope.addOrderFrontProduct();
                            });
                        });
                    return load(promise);
                };

                $scope.$init = function () {
                    $scope.order = Order.init();
                    $scope.$loadAll();
                };

                $scope.$closeOrReset = function (order) {
                    var close = $scope.$$closeOnSave;
                    if (close) {
                        //Tell other tabs that order was edited
                        var key = order.id + ' ' + Date(); //Ensures that each update is unique
                        storage.set(ALTER_ORDER_STORAGE_KEY, key);

                        window.close();
                    }
                    else {
                        $scope.order = Order.init(); //Reset order
                        $scope.order.date = moment(order.date).toDate(); //Preserve order date specified
                        $scope.$timeSearch = '';  //Clear time input
                        $scope.addOrderFrontProduct();
                        $scope.focusFirst = true;
                    }
                };

                $scope.canSendStripeInvoice = function (order) {
                    return Boolean(order.email && order.custom_price);
                }

                // Performs various prompts related to Stripe invoices?
                $scope.$doSendOrResendStripeInvoice = function (order) {
                    let promise = $q.when(order);

                    if ($scope.$sendOrResendStripeInvoice) {
                        if (order.stripe_invoice_id || order.stripe_charge_id) {
                            const isVoid = !order.stripe_charge_id;  // If false, then refund
                            const title = isVoid ? 'Void Order' : 'Refund Order';
                            const message = isVoid
                                ? 'Before sending a new invoice, the current Stripe invoice must be voided. Is this okay? You are responsible to reimbursing the customer if/where needed.'
                                : "Before sending a new invoice, the current Stripe invoice must be refunded. Is this okay? The customer will be issued a refund their credit card.";

                            promise = confirmVoidOrRefundDialog(title, message, order, undefined)
                                .then(() => {
                                    if (isVoid) {
                                        return order.$voidInvoice();
                                    } else {
                                        return order.$refundInvoice();
                                    }
                                });
                        }

                        promise = promise.then(() => {
                            const data = {id: order.id, mark_invoice_as_paid: $scope.$markInvoiceAsPaid};
                            return Order.sendInvoice(data).$promise;
                        });
                    }

                    return promise;
                };

                $scope.saveOrder = function () {
                    let promise = $scope.$saveOrder();

                    promise = load(promise
                        .then(function (order) {
                            return $scope.$doSendOrResendStripeInvoice(order);
                        })
                        .then(function (order) {
                            const invoiceMessage = $scope.$sendOrResendStripeInvoice && "and invoice " || "";
                            const message = `Order ${invoiceMessage}saved successfully!`;
                            $notifier.success(message);
                            $scope.$sendOrResendStripeInvoice = false;
                            $scope.$markInvoiceAsPaid = false;

                            // Open print page, if specified
                            if ($scope.$printOrderAfterSave) {
                                window.open(`/orders/${order.id}/print?close_on_save=true`, "_blank");
                            }

                            return $scope.$closeOrReset(order);
                        }));

                    $utils.$errorCatch(promise);
                    return promise;
                };

                $scope.$init();

                $scope.orderHasFlag = function (flag) {
                    return _.find($scope.order.order_flags, function (flagID) {
                        return flag.id == flagID;
                    });
                };

                $scope.addOrderFlag = function (flag) {
                    $scope.order.order_flags = $scope.order.order_flags || [];
                    $scope.order.order_flags.push(flag.id);
                };

                $scope.removeOrderFlag = function (flag) {
                    $scope.order.order_flags = _.filter($scope.order.order_flags, function (flagID) {
                        return flagID != flag.id;
                    });
                };

                $scope.setToSpecialProduct = (op) => {
                    op.product_type = 'special_product';
                    op.front_product = null;
                };

                $scope.setToFrontProduct = (op) => {
                    op.product_type = 'front_product';
                    op.name = null;
                };

                // Sync $hasSpecialProduct
                $scope.$watch("order.order_products", () => {
                    if ($scope.order) {
                        $scope.$hasSpecialProduct = _($scope.order.order_products)
                            .map((op) => [op, _.find($scope.products, {id: op.front_product})])
                            .some(([op, p]) => op.product_type === "special_product" || (p && p.is_starred));
                    }
                }, true);
            },
            cookieStoreDate: function ($scope) {
                $scope.$saveDate = function () {
                    ipCookie('date', angular.toJson($scope.date), {path: '/', expires: 21});
                };

                $scope.$loadDate = function () {
                    var m = moment(ipCookie('date'));

                    if (m.isValid()) {
                        $scope.date = m.toDate();
                    }
                };

                $scope.$watch('date', $scope.$saveDate);

                $scope.$loadDate();

                $scope.dateSelected = function () {
                    return Boolean($scope.date);
                };
            },
            liveOrderLoader: function ($scope, load, bgLoad, paramDefaults, {onSyncOrders = null} = {}) {
                $scope.date = moment().toDate();
                $orderUtils.cookieStoreDate($scope);

                $scope.$paramDefaults = _.assign({}, paramDefaults);

                $scope.SHOW_ALL_DETAILS_MINIMUM = 3;

                $scope.$resetParams = function () {
                    $scope.params = _.clone($scope.$paramDefaults);
                };
                $scope.$resetParams();

                $scope.$resetQuery = function () {
                    delete $scope.params.q;
                };

                $scope.$addOrderFlag = function (flagID) {
                    $scope.params.orderFlags = ($scope.params.orderFlags || []).concat([flagID]);
                };

                $scope.$removeOrderFlag = function (flagID) {
                    $scope.params.orderFlags = !_.filter($scope.params.orderFlags, function (id) {
                        return id == flagID;
                    });
                };

                $scope.$addOrderFlagOmit = function (flagID) {
                    $scope.params.orderFlagsOmit = ($scope.params.orderFlagsOmit || []).concat([flagID]);
                };

                $scope.$removeOrderFlagOmit = function (flagID) {
                    $scope.params.orderFlagsOmit = !_.filter($scope.params.orderFlagsOmit, function (id) {
                        return id == flagID;
                    });
                };

                $scope.$loadFrontProducts = function () {
                    return FrontProduct.all().then(function (products) {
                        $scope.products = products;
                    });
                };

                $scope.$loadOrderFlags = function () {
                    return OrderFlag.query().$promise.then(function (orderFlags) {
                        $scope.orderFlags = orderFlags;
                    });
                };

                $scope.$loadLocations = function () {
                    return OrderLocation.query().$promise.then(function (locations) {
                        $scope.locations = locations;
                    });
                };

                $scope.$getDate = () => {
                    return $utils.dateToJson($scope.date);
                };

                $scope.$loadOrders = () => {
                    return Order.query({date: $scope.$getDate()}).$promise
                        .then((orders) => {
                            $scope.orders = orders;
                        });
                };

                // Function called when websocket message for order is received
                $scope.$syncOrders = (bridge, message) => {
                    if (message.action == 'subscribe') {
                        return; // no need to do anything on start up
                    }

                    onSyncOrders && onSyncOrders(bridge, message);

                    // Update list according to message
                    $scope.orders = bridge.updateResultList(message, $scope.orders);

                    // Remove orders for another date
                    let dateStr = moment($scope.date).format('YYYY-MM-DD');
                    $scope.orders = _.filter($scope.orders, {date: dateStr});
                };

                $scope.$orderBridge = ModelBridge.connectAndSubscribe(
                    Order, auth.getClient().$getAuthToken(),
                    $scope.$syncOrders
                );
                // Close socket when controller is destroyed
                $scope.$on('$destroy', () => {
                    $scope.$orderBridge.socket.close();
                });

                $scope.$loadFrontProductsNotReady = () => {
                    return FrontProductNotReady.query({date: $scope.$getDate()}).$promise
                        .then((frontProductsNotReady) => {
                            $scope.frontProductsNotReady = frontProductsNotReady;
                        });
                };

                $scope.$loadAll = function () {
                    if ($scope.date) { //Don't load if no date is selected
                        return $q.all([
                            $scope.$loadOrders(),
                            $scope.$loadFrontProductsNotReady(),
                            $scope.$loadFrontProducts(),
                            $scope.$loadOrderFlags(),
                            $scope.$loadLocations()
                        ]);
                    }
                };

                $scope.$reload = new Delayer($scope.$loadAll, {time: 200});
                $scope.$reload.$addLoadFunction(load);

                //Refresh page when params are changed
                $scope.$watch('date', $scope.$reload.invoke);

                $scope.$forceBgLoad = function () {
                    $scope.$reload.invoke();
                };

                $scope.$togglePropertyState = function (obj, prop, state1, state2) {
                    if (obj[prop] === state1) {
                        obj[prop] = state2;
                    }
                    else {
                        obj[prop] = state1;
                    }
                };

                $scope.$onlyOrderEnterEvent = function (action) {
                    if ($scope.$filteredOrders && $scope.$filteredOrders.length == 1) {
                        var order = _.first($scope.$filteredOrders);
                        action(order);
                    }
                };

                $scope.$updateOrderStats = function () {
                    var orders = _($scope.orders);
                    $scope.stats = {
                        total: orders.size(),
                        filled: orders.filter({filled: true}).size(),
                        picked_up: orders.filter({picked_up: true}).size()
                    };
                };
                $scope.$watch('orders', $scope.$updateOrderStats, true);

                $scope.$updateOrderProperties = function () {
                    _.each($scope.orders, function (order) {
                        order.$hasTime = Boolean(order.time);
                        order.$location = _.find($scope.locations, {id: order.location});
                        order.$phoneNumber = $filter('phoneNumber')(order.phone_number);
                        order.$dateStr = $filter('date')(order.date, 'MMM d');
                        order.$timeStr = $filter('date')($filter('parseTime')(order.time), 'shortTime');
                    });
                };
                $scope.$watch('orders', $scope.$updateOrderProperties);
                $scope.$watch('locations', $scope.$updateOrderProperties);

                $scope.$updateOrderFlags = function () {
                    _.each($scope.orders, function (order) {
                        order.$orderFlags = _.map(order.order_flags, (flagID) => {
                            return _.find($scope.orderFlags, {id: flagID});
                        });
                    });
                };
                $scope.$watch('orders', $scope.$updateOrderFlags, true);
                $scope.$watch('orderFlags', $scope.$updateOrderFlags, true);

                $scope.$updateProductsNotReady = function () {
                    _.each($scope.products, function (product) {
                        product.$notReady = _.find($scope.frontProductsNotReady, {front_product: product.id});
                    });
                };
                $scope.$watch('frontProductsNotReady', $scope.$updateProductsNotReady, true);
                $scope.$watch('products', $scope.$updateProductsNotReady, true);
            },
            orderActions: function ($scope, load) {
                $scope.$toggleAttr = function (order, attr) {
                    order[attr] = !order[attr];
                };

                $scope.$orderAttrChange = function (order, func, attr) {
                    $scope.$toggleAttr(order, attr); //Toggle attribute in advance to indicate change to user

                    var regularOrder = new Order(order);

                    return load(regularOrder.save()
                        //If action fails, toggle to original attribute state
                        .catch(function (error) {
                            $scope.$toggleAttr(order, attr);
                            return $q.reject(error); //Continue error propagation
                        }));
                };

                $scope.$pickupOrder = function (order) {
                    return $scope.$orderAttrChange(order, '$pickup', 'picked_up');
                };

                $scope.$returnOrder = function (order) {
                    return $scope.$orderAttrChange(order, '$return', 'picked_up');
                };

                $scope.$fillOrder = function (order) {
                    return $scope.$orderAttrChange(order, '$fill', 'filled');
                };

                $scope.$$selectOrderLocation = function (ev, order) {
                    return $mdDialog.show({
                        controller: 'SelectOrderLocationController',
                        template: renderTemplate(require('./modals/select-order-location.pug')),
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        locals: {
                            locations: $scope.locations
                        }
                    });
                };

                $scope.$fillOrderWithLocationSelect = function (ev, order) {
                    //Before saving, use modal to select order location
                    return $scope.$$selectOrderLocation(ev, order)
                        .then(function (location) {
                            order.filled = true;
                            order.location = location.id;
                            order.$location = location;
                            return $scope.updateOrder(order)
                                .then(function (newOrder) {
                                    $scope.$resetQuery();

                                    newOrder.$location = location;
                                    return newOrder; //Pass updated order to caller
                                });
                        });
                };

                $scope.$selectOrderLocation = function (ev, order) {
                    $scope.$$selectOrderLocation(ev, order)
                        .then(function (location) {
                            order.location = location.id;
                            return $scope.updateOrder(order);
                        });
                };

                $scope.$unFillOrder = function (order) {
                    return $scope.$orderAttrChange(order, '$unFill', 'filled');
                };

                $scope.$markOrderInProgress = function (order) {
                    return $scope.$orderAttrChange(order, '$inProgress', 'in_progress');
                };

                $scope.$markOrderNotInProgress = function (order) {
                    return $scope.$orderAttrChange(order, '$notInProgress', 'in_progress');
                };

                $scope.$verifyOrder = function (order) {
                    return $scope.$orderAttrChange(order, '$verify', 'verified');
                };

                $scope.$unVerifyOrder = function (order) {
                    return $scope.$orderAttrChange(order, '$unVerify', 'verified');
                };

                $scope.$updateOrder = function (order) {
                    var tempOrder = new Order(_.clone(order));

                    // Because detail mode is being used, location, order_flags & order_products
                    // have a different form than expected - correct this before sending!
                    tempOrder.order_products = _.map(tempOrder.order_products, function (ofp) {
                        ofp = _.clone(ofp);
                        ofp.front_product = ofp.front_product && ofp.front_product.id || ofp.front_product;
                        return ofp;
                    });

                    return load(tempOrder.save());
                };

                $scope.$orderPickupMessageHandler = function (ev, order) {
                    return $mdDialog.show({
                        template: renderTemplate(require('./modals/pickup-order-message.pug')),
                        controller: 'PickupOrderMessageController',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        locals: {
                            order: order
                        }
                    });
                };

                $scope.updateOrder = function (order) {
                    return load($scope.$updateOrder(order));
                };

                $scope.toggleFillOrderProduct = function (op, order) {
                    op.filled = !op.filled;
                    return $scope.updateOrder(order);
                };

                $scope.showMoveOrderLocationsDialog = function (ev) {
                    return $mdDialog.show({
                        controller: 'MoveOrderLocationsController',
                        template: renderTemplate(require('./modals/move-order-locations.pug')),
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        locals: {
                            locations: $scope.locations,
                            date: $scope.date,
                        }
                    })
                        .then(() => {
                            $scope.$reload.invoke();
                        });
                };

                $scope.orderIsPaidOrPendingPayment = function (order) {
                    return order.marked_as_paid || order.purchase_status == "paid" || order.purchase_status == "payment_pending";
                }
            },
            showMore: function ($scope) {
                $scope.$resetShowMore = function () {
                    $scope.$showMore = [];
                };

                $scope.$getShowMore = function (order) {
                    return _.find($scope.$showMore, function (id) {
                        return id == order.id;
                    });
                };

                $scope.toggleMoreDetails = function (order) {
                    var existing = $scope.$getShowMore(order);

                    if (existing) {
                        $scope.$showMore.remove(existing);
                    }
                    else {
                        $scope.$showMore.push(order.id);
                    }
                };

                $scope.showMoreDetails = function (order) {
                    var existing = $scope.$getShowMore(order);

                    if (!existing) {
                        $scope.$showMore.push(order.id);
                    }
                };

                $scope.getShowMoreDetails = function (order) {
                    return Boolean($scope.$getShowMore(order));
                };

                $scope.showOrderDetails = function (order, $index) {
                    return $scope.getShowMoreDetails(order) || ($index == 0 && $scope.params.q)
                        || $scope.orders.length <= $scope.SHOW_ALL_DETAILS_MINIMUM;
                };

                //Hide all details when search params are changed
                $scope.$watch('params', function () {
                    $scope.$showMore = [];
                }, true);
            },
            noOrdersFoundSuggestions: function ($scope) {
                $orderUtils.showMore($scope); //Requires showMore functionality

                var suggestLoad = LoaderFactory.silent({property: '$suggestLoad'});
                $scope.MAX_SUGGESTIONS = 8;

                $scope.$sameDate = function (d1, d2) {
                    return $utils.dateEquals($utils.parseDate(d1), d2);
                };

                $scope.$showSuggestions = function () {
                    /**
                     * Indicates if/when the suggestion section should be shown to the user
                     */
                    return !$scope.$suggestLoad && $scope.params.q && $scope.$suggestions;
                };

                $scope.$showForceSuggestions = function () {
                    /**
                     * Indicates if/when the force suggestion section should be shown to the user
                     */
                    return !$scope.$suggestLoad && $scope.params.q
                        && !$scope.$suggestions;
                };

                $scope.$suggestionsFound = function () {
                    return !_.isEmpty($scope.$suggestions && $scope.$suggestions.results);
                };

                $scope.forceShowSuggestions = function () {
                    $scope.$$showSuggestions = true;
                };

                $scope.$resetForceShowSuggestions = function () {
                    $scope.$$showSuggestions = false;
                };

                $scope.$$loadSuggestions = function () {
                    if ($scope.$$showSuggestions) {
                        var params = {
                            q: $scope.params.q,
                            page_size: $scope.MAX_SUGGESTIONS
                        };

                        return Order.paginated(params).$promise.then(function (orders) {
                            $scope.$suggestions = orders;
                        });
                    }
                    else {
                        //Remove old suggestion results
                        $scope.$suggestions = null;
                    }
                };

                $scope.$loadSuggestions = function () {
                    suggestLoad($scope.$$loadSuggestions());
                };

                //Reload suggestions as needed
                $scope.$watch('$$showSuggestions', $scope.$loadSuggestions);

                //Reset $$showSuggestions if query changes
                $scope.$watch('params', $scope.$resetForceShowSuggestions, true);
            },
            orderWarnings: function ($scope, notReadyWarning) {
                $scope.$refreshWarnings = function () {
                    var warnings = [];

                    if ($scope.params.hasTime) {
                        warnings.push('Orders with unknown times are hidden');
                    }

                    if ($scope.params.orderFlags) {
                        warnings.push('Only showing orders with selected flag');
                    }

                    if ($scope.params.orderFlagsOmit) {
                        warnings.push('Excluding showing orders by selected flag');
                    }

                    if ($scope.params.filled == false) {
                        warnings.push('Only showing un-filled orders that can be filled');

                        if (_.size($scope.frontProductsNotReady) && notReadyWarning) {
                            warnings.push('Hiding products not ready');
                        }
                    }

                    if ($scope.params.picked_up == false) {
                        warnings.push('Only showing picked up orders');
                    }

                    $scope.$warnings = warnings;
                };

                $scope.$watch('params', $scope.$refreshWarnings, true);
                $scope.$watch('frontProductsNotReady', $scope.$refreshWarnings);
            },
            tabReOpened: function ($scope, func) {
                $(window).focus(() => {
                    func();
                });
            },
            beginQuickAddOrder: function ($scope, load) {
                $scope.$quickOrderConfirmation = function (ev, order) {
                    return $mdDialog.show({
                        template: renderTemplate(require('./modals/quick-add-order-confirmation.pug')),
                        controller: 'QuickAddOrderConfirmationController',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        locals: {
                            order: order
                        }
                    });
                };


                $scope.showQuickAddOrderModal = function (ev) {
                    //Before saving, use modal to select order location
                    return $mdDialog.show({
                        template: renderTemplate(require('./modals/quick-add-order.pug')),
                        controller: 'QuickAddOrderModalController',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        locals: {
                            date: $scope.date,
                            orderFlags: $scope.orderFlags
                        }
                    })
                        .then(function (order) {
                            if (order.filled) {
                                return $scope.$fillOrderWithLocationSelect(ev, order);
                            }
                            else {
                                return $scope.updateOrder(order);
                            }
                        })
                        .then(function (order) {
                            return $scope.$quickOrderConfirmation(ev, order);
                        })
                        .then($scope.$resetQuery);
                };
            },
            beginCancelOrder: function ($scope, cancelEvent) {
                const load = LoaderFactory.errorOnly();

                $scope.showCancelOrderModal = function (ev, order) {
                    let promise = $q.when(undefined);

                    if (order.stripe_invoice_id || order.stripe_charge_id) {
                        const isVoid = !order.stripe_charge_id;  // If false, then refund
                        const title = isVoid ? 'Void Order' : 'Refund Order';
                        const orderDescription = order.stripe_invoice_id ? "invoiced order" : "online order";
                        const message = isVoid
                            ? `Before cancelling an ${orderDescription}, the Stripe invoice must be voided. Is this okay? You are responsible to reimbursing the customer if/where needed.`
                            : `Before cancelling an ${orderDescription}, the Stripe invoice must be refunded. Is this okay? The customer will be issued a refund their credit card.`;

                        promise = confirmVoidOrRefundDialog(title, message, order, ev)
                            .then(() => {
                                if (isVoid) {
                                    return order.$voidInvoice();
                                } else {
                                    return order.$refundInvoice();
                                }
                            });
                    }

                    const confirmCancel = $mdDialog.confirm()
                        .parent(angular.element(document.body))
                        .title('Confirm Action')
                        .content('Are you sure you wish to cancel this order? This action cannot be reversed!')
                        .ok('Yes')
                        .cancel('No')
                        .targetEvent(ev);

                    return load(promise
                        .then(function () {
                            return $mdDialog.show(confirmCancel);
                        })
                        .then(function () {
                            return order.$cancel();
                        })
                        .then(function () {
                            $notifier.success('Order cancelled successfully!');
                        })
                        .then(cancelEvent));
                };
            },
            paginatedOrders: function ($scope, filterFunc) {
                $scope.$pageSize = 25;

                var filterLoader = LoaderFactory.errorOnly({property: '$filterLoader'});

                // Update $filteredOrders as needed
                $scope.$setFilteredOrders = function () {
                    $scope.$filteredOrders = filterFunc($scope.orders, $scope.params);
                };
                $scope.$filterDelay = new Delayer($scope.$setFilteredOrders, {time: 600});
                $scope.$filterDelay.$addLoadFunction(filterLoader);
                $scope.$watch('orders', $scope.$filterDelay.invoke);
                $scope.$watch('params', $scope.$filterDelay.invoke, true);

                $scope.$resetCurrentCount = function () {
                    $scope.$currentCount = $scope.$pageSize;
                };
                $scope.$watch('$filteredOrders', $scope.$resetCurrentCount);

                $scope.$showMoreOrders = function () {
                    if ($scope.$currentCount <= $scope.orders.length && !$scope.$pageThrottle) {
                        $timeout(function () { //Add slight delay before changing current count - ensures loader is shown
                            $scope.$currentCount += $scope.$pageSize;
                        });

                        $scope.$pageThrottle = true;
                        $timeout(function () {
                            $scope.$pageThrottle = false;
                        }, 300);
                    }
                };

                $scope.$setPaginatedOrders = function () {
                    var orders = $scope.$filteredOrders || [];
                    $scope.$paginatedOrders = $filter('limitTo')(orders, $scope.$currentCount);
                    $scope.$hasMoreOrders = $scope.$paginatedOrders.length != orders.length;
                };
                $scope.$watch('$filteredOrders', $scope.$setPaginatedOrders);
                $scope.$watch('$currentCount', $scope.$setPaginatedOrders);
            },
            printCommon: function ($scope) {
                // Split order products into different order slips, as needed
                $scope.$updateOrderSlipMap = (orders) => {
                    let pageNo = 0;
                    $scope.orderSlipMap = {};
                    _.each(orders, (order) => {
                        // Spread products evenly across all order slips
                        let orderSlipProducts;
                        if (order.order_products.length > 0) {
                            orderSlipProducts = _.chunkEvenly(order.order_products, 12)
                        }
                        else {
                            // If no products in order, still display one order slip
                            orderSlipProducts = [[]];
                        }

                        $scope.orderSlipMap[order.id] = _.map(orderSlipProducts, (products, index) => {
                            pageNo += 1;
                            return {
                                $id: `${order.id}-${index}`,
                                $pageNo: pageNo,
                                products: products,
                            }
                        });
                        $scope.totalNumPages = pageNo;
                    });
                }
            }
        };

        return $orderUtils;
    });