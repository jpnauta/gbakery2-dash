export const DashOrdersModalsModule = angular.module('dash.orders.modals', [])
    .controller('PickedUpOrderWarningController', function ($scope, $mdDialog, order) {
        $scope.order = order;

        $scope.ok = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    })
    .controller('PickupOrderMessageController', function ($scope, $mdDialog, order) {
        $scope.order = order;

        $scope.ok = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    })
    .controller('ProductsNotReadyModalController', function ($scope, $mdDialog, date, $utils, products, $q,
                                                             LoaderFactory, FrontProductNotReady, _) {
        $scope.params = {};

        var load = LoaderFactory.errorOnly({scope: $scope});

        $scope.date = date;
        $scope.products = products;

        $scope.$setProductNotReady = function (fpID, date) {
            var notReady = new FrontProductNotReady({front_product: fpID, date: date});

            return notReady.save();
        };

        $scope.$deleteProductNotReady = function (id) {
            return new FrontProductNotReady({id: id}).$delete();
        };

        $scope.toggleProductNotReady = function (product) {
            var promise;
            if (product.$available) {
                promise = $scope.$deleteProductNotReady(product.$notReady.id);
            } else {
                promise = $scope.$setProductNotReady(product.id, $scope.date);
            }

            return load(promise);
        };

        $scope.ok = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        // Update product.$available as needed
        $scope.$updateProductAvailable = function () {
            _.each($scope.products, function (product) {
                product.$available = !product.$notReady;
            });
        };
        $scope.$watch('products', $scope.$updateProductAvailable, true);
    })
    .controller('QuickAddOrderModalController', function ($scope, $mdDialog, $utils, LoaderFactory, $constants,
                                                          date, orderFlags, Order, $notifier, $orderUtils, _) {
        var load = LoaderFactory.errorOnly();

        $scope.$timeOptions = $utils.createStdTimeSets();
        $scope.$focusFirst = true;

        $scope.order = Order.init();
        $scope.order.date = date;

        $scope.orderFlags = orderFlags;

        $orderUtils.newEditUtils($scope);

        $scope.saveQuickOrder = function () {
            //Add prefix to comment to indicate quick order
            var comment = 'QUICK ORDER';
            if ($scope.order.comment) {
                comment += ' - ' + $scope.order.comment;
            }
            $scope.order.comment = comment;

            var order = $scope.$cleanOrder($scope.order);
            $mdDialog.hide(order);
        };

        //Add blank special product if where/needed
        $scope.$watch('order', function (order) {
            var ops = order && order.order_products,
                last = _.last(ops);

            if (!last || $scope.ofpHasUserInput(last)) {
                $scope.addSpecialProduct();
            }
        }, true);

        $scope.orderHasFlag = function (flag) {
            return _.find($scope.order.order_flags, function (flagID) {
                return flag.id == flagID;
            });
        };

        $scope.addOrderFlag = function (flag) {
            $scope.order.order_flags = $scope.order.order_flags || [];
            $scope.order.order_flags.push(flag.id);
        };

        $scope.removeOrderFlag = function (flag) {
            $scope.order.order_flags = _.filter($scope.order.order_flags, function (flagID) {
                return flagID != flag.id;
            });
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.$quantityTypes = $constants.QUANTITY_TYPE_LIST;
    })
    .controller('QuickAddOrderConfirmationController', function ($scope, $mdDialog, order) {
        $scope.order = order;

        $scope.ok = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    })
    .controller('SelectOrderLocationController', function ($scope, $mdDialog, locations, _) {
        $scope.locations = locations;
        $scope.$focusOrderLocationID = true;

        $scope.chooseOrderLocation = function (location) {
            $mdDialog.hide(location);
        };

        $scope.findOrderLocationByID = function (hotkey) {
            hotkey = (hotkey || '').toLowerCase();

            var location = _.find($scope.locations, function (location) {
                return hotkey == location.hotkey.toLowerCase();
            });

            if (location) {
                $scope.chooseOrderLocation(location);
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    })
    .controller('MoveOrderLocationsController', function ($scope, $mdDialog, locations, date, OrderLocation, LoaderFactory, moment) {
        $scope.locations = locations;
        $scope.data = {
            date: moment(date).format('YYYY-MM-DD'),
            src_location: null,
            dest_location: null,
        };

        const load = LoaderFactory.errorOnly({scope: $scope});

        $scope.moveOrderLocations = function (location) {
            const promise = OrderLocation.move($scope.data).$promise.then(function (categories) {
                $scope.categories = categories;
            });

            load(promise);

            promise.then(() => {
                $mdDialog.hide(location);
            });
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    });