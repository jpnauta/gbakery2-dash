import { DashGenericModule } from "../generic";
import { DashOrdersUtilsModule } from "./utils";
import { DashOrdersModelsModule } from "./models";
import { DashOrdersFiltersModule } from "./filters";
import { DashOrdersControllersModule } from "./controllers";
import { DashOrdersModalsModule } from "./modals";

export const DashOrdersModule = angular.module('dash.orders', [
  DashGenericModule.name, DashOrdersUtilsModule.name, DashOrdersModelsModule.name,
  DashOrdersFiltersModule.name, DashOrdersControllersModule.name, DashOrdersModalsModule.name
]);