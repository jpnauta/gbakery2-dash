export const DashOrdersModelsModule = angular.module('dash.orders.models', [])
    .factory('$orderCommon', function ($utils, _) {
        return function (OrderClass) {

            //Override save function to format date correctly during save
            var orig = OrderClass.prototype.save;
            OrderClass.prototype.save = function () {
                var params = _.clone(this);
                params.date = $utils.dateToJson(this.date);

                return orig.apply(new OrderClass(params), arguments);
            };

            var STATUSES = {
                PICKED_UP: {
                    name: 'Picked up',
                    bgClass: 'bg-gray-50',
                    iconClass: 'fa-check'
                },
                FILLED: {
                    name: 'Filled',
                    bgClass: 'bg-green-50',
                    iconClass: 'fa-thumbs-up'
                },
                CANNOT_BE_FILLED: {
                    name: 'Cannot be filled',
                    bgClass: 'bg-red-50',
                    iconClass: 'fa-ban'
                },
                IN_PROGRESS: {
                    name: 'In Progress',
                    bgClass: 'bg-light-blue-50',
                    iconClass: 'fa-user'
                },
                CANCELLED: {
                    name: 'Cancelled',
                    bgClass: 'bg-red-50',
                    iconClass: 'fa-cross'
                },
                NOT_FILLED: {
                    name: 'Not filled',
                    bgClass: 'bg-yellow-50',
                    iconClass: 'fa-times'
                }
            };

            OrderClass.prototype.$status = function () {
                if (this.picked_up) {
                    return STATUSES.PICKED_UP;
                }
                else if (this.filled) {
                    return STATUSES.FILLED;
                }
                else if (this.cannot_be_filled) {
                    return STATUSES.CANNOT_BE_FILLED;
                }
                else if (this.in_progress) {
                    return STATUSES.IN_PROGRESS;
                }
                else if (this.cancelled) {
                    return STATUSES.CANCELLED;
                }
                else {
                    return STATUSES.NOT_FILLED;
                }
            };

            //OrderClass.prototype.$verifiedStatus = function () {
            //    if (this.verified) {
            //        return {
            //            name: 'Verified',
            //            bgClass: 'verified'
            //        };
            //    }
            //    else {
            //        return {
            //            name: 'Not Verified',
            //            bgClass: 'unverified'
            //        };
            //    }
            //};
        };
    })
    .factory('Order', function (ModelFactory, $utils, $orderCommon, _) {
        var $orderAction = function (listCtrl) {
            return {
                method: 'PUT',
                params: {
                    listCtrl: listCtrl
                }
            };
        };

        var Order = ModelFactory({
            channel: 'dashboard/orders',
            baseUrl: '/api/dashboard/orders/',
            actions: {
                stats: {
                    method: 'GET',
                    params: {
                        listCtrl: 'stats'
                    }
                },
                bulkAll: {
                    method: 'GET',
                    params: {
                        listCtrl: 'all'
                    },
                    isArray: true
                },

                //Order actions
                pickup: $orderAction('pickup'),
                'return': $orderAction('return'),
                fill: $orderAction('fill'),
                unFill: $orderAction('un-fill'),
                inProgress: $orderAction('in-progress'),
                notInProgress: $orderAction('not-in-progress'),
                verify: $orderAction('verify'),
                unVerify: $orderAction('un-verify'),
                cancel: $orderAction('cancel'),
                sendInvoice: $orderAction('send-invoice'),
                resendInvoice: $orderAction('resend-invoice'),
                voidInvoice: $orderAction('void-invoice'),
                refundInvoice: $orderAction('refund-invoice'),
            },
            paginated: false,
        });

        $orderCommon(Order); //Apply common order functions to Order class

        //Override save function to format date correctly during save
        var orig = Order.prototype.save;
        Order.prototype.save = function () {
            var params = _.clone(this);
            params.date = $utils.dateToJson(this.date);

            return orig.apply(new Order(params), arguments);
        };

        Order.all = function (opts) {
            opts = _.assign({
                details: true //Tells the server to load the actual front product of order front products
            }, opts);

            return Order.query(opts).$promise;
        };

        Order.unPaginated = function (opts) {
            opts = _.assign({
                details: true //Tells the server to load the actual front product of order front products
            }, opts);

            return Order.bulkAll(opts).$promise;
        };

        Order.init = function () {
            return new Order({
                special_products: [],
                order_flags: [],
                date: null
            });
        };

        return Order;
    })
    .factory('OrderFlag', function (ModelFactory) {
        return ModelFactory({
            baseUrl: '/api/dashboard/orders/flags/',
            paginated: false
        });
    })
    .factory('OrderLocation', function (ModelFactory) {
        return ModelFactory({
            baseUrl: '/api/dashboard/orders/locations/',
            paginated: false,
            actions: {
                move: {
                    method: 'POST',
                    params: {
                        listCtrl: 'move'
                    }
                }
            }
        });
    })
    .factory('OrderProduct', function (ModelFactory) {
        return ModelFactory({
            baseUrl: '/api/dashboard/orders/products/',
            paginated: true,
            actions: {
                stats: {
                    method: 'GET',
                    params: {
                        listCtrl: 'stats'
                    },
                    isArray: true
                }
            }
        });
    })
    .factory('SpecialProduct', function (ModelFactory) {
        return ModelFactory({
            baseUrl: '/api/dashboard/special-products/',
            paginated: false,
            actions: {
                report: {
                    method: 'GET',
                    params: {
                        listCtrl: 'report'
                    },
                    isArray: true
                }
            }
        });
    })
    .factory('SyncDashSpecialProduct', function (SyncModelFactory) {
        return SyncModelFactory({
            syncUrl: 'dashboard/orders/special-products',
            baseUrl: '/api/sync/dashboard/orders/special-products/',
            paginated: false
        });
    });