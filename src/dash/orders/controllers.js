import { renderTemplate } from "../../core/generic/templates";
import ngFileUpload from "ng-file-upload";
import Papa from "papaparse";

export const DashOrdersControllersModule = angular.module('dash.orders.controllers', [ngFileUpload])
    .controller('EditOrderController', function ($scope, $orderUtils, $routeParams, _) {
        $scope.$$id = Number($routeParams.id);
        $scope.$$closeOnSave = _.toBoolean($routeParams.close_on_save);
        $orderUtils.newEditCommon($scope);
    })
    .controller('FillOrderController', function ($scope, $utils, $notifier, LoaderFactory, $orderUtils, OrderFlag, $mdDialog,
                                                 $filter, _) {
        var load = LoaderFactory.errorOnly();
        var bgLoad = LoaderFactory.errorOnly({property: '$bgLoading'});
        var defaultParams = {
            filled: false
        };

        const onSyncOrders = (bridge, message) => {
            //If order is picked up before being filled, show warning message
            if (message.action == 'update') {
                const order = message.data;
                const oldOrder = _.find($scope.orders, {id: order.id});

                if (oldOrder && !oldOrder.filled && !oldOrder.picked_up && order.picked_up) {
                    // Show warning dialog
                    $mdDialog.show({
                        template: renderTemplate(require('./modals/picked-up-order-warning.pug')),
                        controller: 'PickedUpOrderWarningController',
                        parent: angular.element(document.body),
                        clickOutsideToClose: true,
                        locals: {
                            order: oldOrder
                        }
                    }).then(function () {
                        // Mark order as filled, if indicated by user
                        $scope.$fillOrder(oldOrder);
                    });
                }
            }
        };

        //Define common functionality for live order pages
        $orderUtils.liveOrderLoader($scope, load, bgLoad, defaultParams, {onSyncOrders});
        $orderUtils.orderActions($scope, bgLoad);
        $orderUtils.noOrdersFoundSuggestions($scope);
        $orderUtils.orderWarnings($scope, true);
        $orderUtils.paginatedOrders($scope, function (orders, params) {
            return $filter('fillOrderFilter')(orders, params);
        });
        $orderUtils.beginCancelOrder($scope, $scope.$reload.invoke);

        $orderUtils.beginQuickAddOrder($scope, load);

        $scope.toggleFillOrderVisible = function () {
            $scope.$togglePropertyState($scope.params, 'filled', false, null);
            $scope.params.with_time_only = undefined; //Don't hide orders without times
        };

        $scope.toggleWithTimeOnly = function () {
            $scope.$togglePropertyState($scope.params, 'hasTime', true, undefined);
        };

        $scope.markOrderInProgress = function (order) {
            $scope.$markOrderInProgress(order);
        };

        $scope.markOrderNotInProgress = function (order) {
            $scope.$markOrderNotInProgress(order);
        };

        $scope.fillOrder = function (event, order) {
            $scope.$fillOrderWithLocationSelect(event, order);
        };

        $scope.unFillOrder = function (order) {
            return $scope.$unFillOrder(order);
        };

        $scope.pickupOrder = function (order) {
            $scope.$pickupOrder(order);
        };

        $scope.returnOrder = function (order) {
            return $scope.$returnOrder(order);
        };

        $scope.resetAndRefresh = function () {
            $scope.$resetParams();
            $scope.$forceBgLoad();
        };

        $scope.showProductsNotReadyDialog = function (ev) {
            $scope.$loadFrontProductsNotReady()
                .then(() => {
                    return $mdDialog.show({
                        template: renderTemplate(require('./modals/products-not-ready.pug')),
                        controller: 'ProductsNotReadyModalController',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        locals: {
                            date: $scope.date,
                            products: $scope.products
                        }
                    });
                });
        };

        $scope.selectOrderLocation = function (ev, order) {
            $scope.$selectOrderLocation(ev, order);
        };
    })
    .filter('fillOrderFilter', function ($filter, _) {
        return function (list, params) {
            //Filter by filled, if specified
            if (_.isBoolean(params.filled)) {
                list = _.filter(list, function (order) {
                    return order.filled == params.filled && (order.cannot_be_filled == false || order.picked_up);
                });
            }

            //Filter out orders without times, if specified
            if (_.isBoolean(params.hasTime)) {
                list = $filter('filter')(list, {$hasTime: params.hasTime});
            }

            //Filter orders with the specified order flag, if specified
            if (params.orderFlags) {
                list = _.filter(list, function (order) {
                    return _.find(order.order_flags, function (flagID) {
                        return _.contains(params.orderFlags, flagID);
                    });
                });
            }

            //Filter orders with the specified order flag, if specified
            if (params.orderFlagsOmit) {
                list = _.filter(list, function (order) {
                    return !_.find(order.order_flags, function (flagID) {
                        return _.contains(params.orderFlagsOmit, flagID);
                    });
                });
            }

            // Text search
            list = $filter('filter')(list, params.q);

            // Sort results
            list = $filter('orderBy')(list, ['-picked_up', '-in_progress', '$hasTime', 'time', 'reference_id']);

            return list;
        };
    })
    .controller('OrderListController', function ($scope, $utils, Order, LoaderFactory, Delayer,
                                                 $constants, $orderUtils, _, pageManager, OrderFlag) {
        var load = LoaderFactory.errorOnly();

        $scope.params = {page_size: 10};

        $scope.$loadOrders = context => {
            return Order.paginated(_.assign(context, $scope.params));
        };

        $scope.$loadOrderFlags = function () {
            return OrderFlag.query().$promise.then(function (orderFlags) {
                $scope.orderFlags = orderFlags;
            });
        };

        $scope.$updateOrderFlags = function () {
            if ($scope.orderManager.page) {
                _.each($scope.orderManager.page.results, function (order) {
                    order.$orderFlags = _.map(order.order_flags, (flagID) => {
                        return _.find($scope.orderFlags, {id: flagID});
                    });
                });
            }
        };
        $scope.$watch('orderManager.page.results', $scope.$updateOrderFlags);
        $scope.$watch('orderFlags', $scope.$updateOrderFlags);

        $scope.$loadOrderFlags();

        $scope.orderManager = pageManager($scope.$loadOrders, {
            page_size: 5
        });

        $scope.$reload = new Delayer(() => $scope.orderManager.start());
        $scope.$reload.$addLoadFunction(load);

        //Reload page after order is edited
        $orderUtils.tabReOpened($scope, $scope.$reload.invoke);
        $orderUtils.beginCancelOrder($scope, $scope.$reload.invoke);

        $scope.orderIsPaidOrPendingPayment = function (order) {
            return order.marked_as_paid || order.purchase_status == "paid" || order.purchase_status == "payment_pending";
        }

        //Reload data on search params change
        $scope.$watch('params', function () {
            $scope.$reload.invoke(); //Invoke page reload
        }, true);
    })
    .controller('NewOrderController', function ($scope, $orderUtils) {
        $orderUtils.newEditCommon($scope);
    })
    .controller('PickupOrderController', function ($scope, $utils, $notifier, LoaderFactory, $orderUtils, $filter) {
        var load = LoaderFactory.errorOnly();
        var bgLoad = LoaderFactory.errorOnly({property: '$bgLoading'});

        //Define common functionality for live order pages
        $orderUtils.liveOrderLoader($scope, load, bgLoad);
        $orderUtils.orderActions($scope, bgLoad);
        $orderUtils.showMore($scope);
        $orderUtils.noOrdersFoundSuggestions($scope);
        $orderUtils.orderWarnings($scope);
        $orderUtils.paginatedOrders($scope, function (orders, params) {
            return $filter('pickupOrderFilter')(orders, params);
        });
        $orderUtils.beginCancelOrder($scope, $scope.$reload.invoke);

        $orderUtils.beginQuickAddOrder($scope, load);

        $scope.togglePickedUp = function () {
            //Toggle between false (=hide picked up orders) or undefined (=don't filter at all)
            $scope.$togglePropertyState($scope.params, 'picked_up', false, undefined);
        };

        $scope.pickupOrder = function (ev, order) {
            $scope.$pickupOrder(order);
            return $scope.$orderPickupMessageHandler(ev, order).then($scope.$resetQuery);
        };

        $scope.returnOrder = function (order) {
            return $scope.$returnOrder(order).then(function () {
                $notifier.success('Order returned!');
            });
        };

        $scope.fillOrder = function (order) {
            return $scope.$fillOrder(order).then(function () {
                $notifier.success('Order marked as filled!');
            });
        };

        $scope.unFillOrder = function (order) {
            return $scope.$unFillOrder(order).then(function () {
                $notifier.success('Order unmarked as filled!');
            });
        };

        $scope.selectOrderLocation = function (ev, order) {
            $scope.$selectOrderLocation(ev, order);
        };

        //Reset search when user has been idle for a while
        $scope.$on('$idleStart', function () {
            $scope.$resetQuery();
            $scope.$resetShowMore();
        });

        //Close details sections when query is changed
        $scope.$watch('params.q', function () {
            $scope.$resetShowMore();
        });

        $scope.pickupOrderEnterEvent = function () {
            $scope.$onlyOrderEnterEvent(function (order) {
                if (!order.picked_up) {
                    $scope.pickupOrder(order);
                }
                else {
                    $scope.returnOrder(order);
                }
            });
        };
    })
    .filter('pickupOrderFilter', function ($filter, _) {
        return function (list, params) {

            //Filter by filled, if specified
            if (_.isBoolean(params.picked_up)) {
                list = $filter('filter')(list, {picked_up: params.picked_up});
            }

            //Filter orders with the specified order flag, if specified
            if (params.order_flags) {
                list = _.filter(list, function (order) {
                    return _.includes(order.order_flags, params.order_flags);
                });
            }

            // Text search
            list = $filter('filter')(list, params.q);

            // Sort results
            list = $filter('orderBy')(list, ['name']);

            return list;
        };
    })
    .controller('StatsOrderController', function ($scope, $utils, $constants, $notifier, $q, Delayer, Repeater, LoaderFactory, $orderUtils, OrderProduct, moment) {
        var load = LoaderFactory.errorOnly();
        var bgLoad = LoaderFactory.errorOnly({property: '$bgLoading'});

        $scope.REFRESH_FREQUENCY = 120 * 1000;

        $orderUtils.cookieStoreDate($scope);

        $scope.params = {};

        $scope.$loadOrderProductStats = function () {
            const params = {order__date: moment($scope.date).format('YYYY-MM-DD')};
            return OrderProduct.stats(params).$promise.then(function (productStats) {
                $scope.productStats = productStats;
            });
        };

        $scope.$loadAll = function () {
            if ($scope.date) { //Don't load if no date is selected
                return load($q.all([
                    $scope.$loadOrderProductStats(),
                ]));
            }
        };

        $scope.$reload = new Delayer($scope.$loadAll, {time: 200});
        $scope.$reload.$addLoadFunction(load);

        $scope.$watch('date', $scope.$reload.invoke);
        $scope.$repeater = new Repeater(function () {
            bgLoad($scope.$loadAll());
        }, {time: $scope.REFRESH_FREQUENCY}).start();

        $scope.$quantityType = function (quantity_type) {
            return $constants.QUANTITY_TYPES_BY_ID[quantity_type];
        };

        $scope.$productQuantity = function (quantity, type) {
            return quantity / $scope.$quantityType(type).quantity;
        };
    })
    .controller('VerifyOrderController', function ($scope, $utils, $notifier, LoaderFactory, $orderUtils, $filter) {
        var load = LoaderFactory.errorOnly();
        var bgLoad = LoaderFactory.errorOnly({property: '$bgLoading'});

        //Define common functionality for live order pages
        $orderUtils.liveOrderLoader($scope, load, bgLoad, {}, {});
        $orderUtils.orderActions($scope, bgLoad);
        $orderUtils.showMore($scope);
        $orderUtils.noOrdersFoundSuggestions($scope);
        $orderUtils.paginatedOrders($scope, function (orders, params) {
            return $filter('verifyOrderFilter')(orders, params);
        });
        $orderUtils.beginCancelOrder($scope, $scope.$reload.invoke);

        $scope.toggleVerified = function () {
            $scope.$togglePropertyState($scope.params, 'verified', false, undefined);
        };

        $scope.verifyOrder = function (order) {
            return $scope.$verifyOrder(order).then(function () {
                $notifier.success('Order verified!');
                $scope.$resetQuery();
            });
        };

        $scope.unVerifyOrder = function (order) {
            return $scope.$unVerifyOrder(order).then(function () {
                $notifier.success('Order un-verified!');
                $scope.$resetQuery();
            });
        };

        $scope.verifyOrderEnterEvent = function () {
            $scope.$onlyOrderEnterEvent(function (order) {
                if (!order.verified) {
                    $scope.verifyOrder(order);
                }
                else {
                    $scope.unVerifyOrder(order);
                }
            });
        };
    })
    .filter('verifyOrderFilter', function ($filter, _) {
        return function (list, params) {
            // Text search
            list = $filter('filter')(list, params.q);

            //Filter by filled, if specified
            if (_.isBoolean(params.verified)) {
                list = $filter('filter')(list, {verified: params.verified});
            }

            // Sort results
            list = $filter('orderBy')(list, ['name']);

            return list;
        };
    })
    .controller('PrintOrderController', function ($scope, $q, $utils, LoaderFactory, Order, OrderFlag, $orderUtils, _, $routeParams, $constants, $timeout) {
        $scope.$$id = Number($routeParams.id);
        $scope.$$closeOnSave = _.toBoolean($routeParams.close_on_save);

        $orderUtils.printCommon($scope);

        var load = LoaderFactory.errorOnly();

        $scope.PRINT_METHOD_OPTIONS = $constants.PRINT_METHOD_OPTIONS;
        $scope.PRINT_METHOD_LIST = $constants.PRINT_METHOD_LIST;
        $scope.printMethod = $scope.PRINT_METHOD_OPTIONS["PRINT_THERMAL"];

        $scope.$loadOrder = function (id) {
            return load(Order.get({id: id}).$promise.then(function (order) {
                $scope.order = order;
            }));
        };

        window.addEventListener("afterprint", () => {
            if ($scope.$$closeOnSave) {
                window.close();
            }
        });

        $scope.$loadAll = function () {
            return load($scope.$loadOrder($scope.$$id));
        };

        $scope.$watch("order", () => {
            if ($scope.order) {
                $scope.$updateOrderSlipMap([$scope.order]);

                $timeout(function () {
                    // Begin print once page is rendered
                    window.print();
                });
            }
        });

        $scope.$loadAll();
    });