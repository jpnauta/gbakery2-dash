export const DashOrdersFiltersModule = angular.module('dash.orders.filters', [])
    .filter('orderFrontProductDisplay', function ($constants) {
        return function (ofp) {
            if (ofp) {
                var fpName = ofp.front_product && ofp.front_product.name || '(unknown)';

                return ofp.quantity + ' ' +
                    $constants.QUANTITY_TYPES_BY_ID[ofp.quantity_type].shortName2 + ' ' + fpName;
            }
        };
    })
    .filter('orderLocationDisplay', function () {
        return function (ol) {
            if (ol) {
                return ol.description + ' (' + ol.hotkey + ')';
            }
        };
    })
    .filter('timeSortFilter', function ($number, $filter, _) {
        var $match = function (str, q) {
            return (str || '').startsWith(q);
        };

        return function (list, q) {
            //Perform ordinary filter
            var filtered = $filter('filter')(list, q);

            //Show most relevant times first
            _.each(list, function (obj) {
                obj.$$match = $match(obj.str12h, q) || $match(obj.str12hShort, q);
            });
            return $filter('orderBy')(filtered, '-$$match');
        };
    })
    .filter('specialProductDisplay', function () {
        return function (sp) {
            if (sp) {
                return sp.description;
            }
        };
    });