import { renderTemplate } from "../core/generic/templates";

export const DashRoutesModules = angular.module('dash.routes', [])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                template: renderTemplate(require('./orders/index.pug')),
                data: {
                    pageTitle: 'All Orders',
                },
            })
            .when('/orders/pickup', {
                template: renderTemplate(require('./orders/pickup.pug')),
                data: {
                    pageTitle: 'Pickup Orders',
                },
            })
            .when('/orders/stats', {
                template: renderTemplate(require('./orders/stats.pug')),
                data: {
                    pageTitle: 'Order Stats',
                },
            })
            .when('/orders/verify', {
                template: renderTemplate(require('./orders/verify.pug')),
                data: {
                    pageTitle: 'Verify Orders',
                },
            })
            .when('/orders/fill', {
                template: renderTemplate(require('./orders/fill.pug')),
                data: {
                    pageTitle: 'Fill Orders',
                },
            })
            .when('/orders/new', {
                template: renderTemplate(require('./orders/new.pug')),
                data: {
                    pageTitle: 'New Order',
                },
            })
            .when('/orders/:id/edit', {
                template: renderTemplate(require('./orders/edit.pug')),
                data: {
                    pageTitle: 'Edit Order',
                },
            })
            .when('/reports/products', {
                template: renderTemplate(require('././reports/products.pug')),
                data: {
                    pageTitle: 'Product Report',
                },
            })
            .when('/reports/orders', {
                template: renderTemplate(require('./reports/orders.pug')),
                data: {
                    pageTitle: 'Order Report',
                },
            })
            .when('/users/logout', {
                template: renderTemplate(require('./users/logout.pug')),
                data: {
                    pageTitle: 'Logout',
                },
            })
            .when('/users/login', {
                template: renderTemplate(require('./users/login.pug')),
                data: {
                    pageTitle: 'Login',
                },
            })
            .when('/orders/:id/print', {
                template: renderTemplate(require('./orders/print.pug')),
                data: {
                    pageTitle: 'Print Order',
                },
            })
            .when('/bocf/tools', {
                template: renderTemplate(require('./bocf/tools.pug')),
                data: {
                    pageTitle: 'Tools for Best of Calgary Foods',
                },
            })
            .otherwise({
                template: renderTemplate(require('./errors/404.pug')),
                data: {
                    pageTitle: '404 Not Found',
                },
            });
    });