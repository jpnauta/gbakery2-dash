import ngFileUpload from "ng-file-upload";
import bocfScript from "./bocf-script.js.txt";

export const DashBocfControllersModule = angular.module('bocf.orders.controllers', [ngFileUpload])
    .controller('BOCFToolsOrderController', function ($scope, $orderUtils, _, LoaderFactory, $constants) {
        // This script is generated using the `gbakery2-bocf` project and pasted into `bocf-script.js.txt`
        // https://bitbucket.org/jpnauta/gbakery2-bocf/src/master/
        $scope.bocfScript = "// Click here to select all!\n" + bocfScript;
        $scope.bocfOutput = null;
        $scope.selectedIndex = 0;

        var load = LoaderFactory.errorOnly();

        $scope.PRINT_METHOD_OPTIONS = $constants.PRINT_METHOD_OPTIONS;
        $scope.printMethod = $scope.PRINT_METHOD_OPTIONS["PRINT_THERMAL"];

        $scope.orderConfirmationSlips = [];

        // TODO remove?
        // $scope.parseCSV = (file, options = {}) => {
        //     return new Promise((resolve, reject) => {
        //         Papa.parse(file, {
        //             complete: resolve,
        //             error: reject,
        //             ...options
        //         });
        //     })
        // }
        //
        // $scope.onFileSelected = function (files) {
        //     const file = files[0];
        //     load($scope.parseCSV(file, {header: true})
        //         .then(({data: rows}) => {
        //             $scope.orders = _(rows)
        //                 // Filter out last row with missing order number
        //                 .filter((row) => row["Order No"])
        //                 .map((row) => {
        //                     return {
        //                         bocf_order_no: row["Order No"],
        //                         name: row["Shipping Full Name"],
        //                         phone_number: row["Billing Phone"],
        //                         email: row["Billing Email"],
        //                         comment: row["Customer Note"],
        //                         order_products: _(row["Order Items"])
        //                             .split("; ")
        //                             .map((productName) => {
        //                                 return {
        //                                     name: productName,
        //                                 };
        //                             })
        //                             .value()
        //                     }
        //                 })
        //                 .value();
        //         }));
        // };

        $scope.print = function () {
            window.print();
        };

        $scope.$watch("bocfOutput", () => {
            $scope.orderConfirmationSlips = [];
            $scope.orderTotals = [];
            $scope.jsonParseError = false;
            if ($scope.bocfOutput) {
                try {
                    $scope.orderConfirmationSlips = JSON.parse($scope.bocfOutput);
                }
                catch (e) {
                    $scope.jsonParseError = true;
                }

                if ($scope.orderConfirmationSlips.length) {
                    $scope.selectedIndex = 1;
                    $scope.orderTotals = _($scope.orderConfirmationSlips)
                        .groupBy("deliveryDay")
                        .map((deliveryDayOrders, deliveryDay) => {
                            return {
                                deliveryDay,
                                productTotals: _(deliveryDayOrders)
                                    .map("orderProducts")
                                    .flatten()
                                    .groupBy("name")
                                    .map((orderProducts, productName) => {
                                        return {
                                            name: productName,
                                            quantity: _.sum(orderProducts, "quantity"),
                                        };
                                    })
                                    .orderBy("name")
                                    .value()
                            };
                        })
                        .value();

                    // Build map {"[delivery date]": true, ...}
                    $scope.deliveryDaysEnabled = _($scope.orderConfirmationSlips)
                        .map("deliveryDay")
                        .uniq()
                        .keyBy()
                        .mapValues(() => true)
                        .value();
                }
            }
        });

        $scope.hideDeliveryDay = (deliveryDay) => {
            $scope.deliveryDaysEnabled[deliveryDay] = false;
        };
    });