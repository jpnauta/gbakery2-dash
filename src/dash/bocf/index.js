import {DashBocfControllersModule} from "./controllers";

export const DashBocfModule = angular.module('dash.bocf', [
    DashBocfControllersModule.name,
]);
