import { CoreAccountsAuthorizationModule } from "../../core/accounts/authorization";
import "./login.scss";

export const DashUsersLoginModule = angular.module('dash.users.login', [CoreAccountsAuthorizationModule.name])
    .controller('LoginController', function ($scope, auth, LoaderFactory, $location, userSession) {
      var load = LoaderFactory.errorOnly();

      $scope.doLogin = () => {
        var promise = auth.login($scope.data.email, $scope.data.password)
            .then(() => {
              // Reload user session data
              userSession.clear();
              return userSession.load();
            })
            .then(() => {
              // Redirect to appropriate page
              let search = $location.search();
              let redirect = search.redirect || '/';
              $location.url(redirect);
            });

        load(promise);

        return promise;
      };
    });