import { CoreAccountsAuthorizationModule } from "../../core/accounts/authorization";

export const DashUsersLogoutModule = angular
    .module('dash.users.logout', [CoreAccountsAuthorizationModule.name])
    .controller('LogoutController', function ($scope, auth, LoaderFactory, $location) {
      // Logout from server
      auth.logout()
          .then(() => {
            // Redirect to login page
            $location.url('/users/login').replace();
          });
    });