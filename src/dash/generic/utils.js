import { CoreGenericErrorsModule } from "../../core/generic/errors";
import { CoreGenericLoadingModule } from "../../core/generic/loading";
import { CoreGenericMessagesModule } from "../../core/generic/messages";
import { CoreExtJquery } from "../../core/ext/jquery";

export const DashGenericUtilsModule = angular.module('dash.generic.utils', [CoreGenericLoadingModule.name, CoreGenericMessagesModule.name, CoreGenericErrorsModule.name, CoreExtJquery.name])
    .factory('$utils', function ($http, $q, Loading, $notifier, $window, errorUtils, _, moment, $) {

        var $utils = {
            redirectTo: function (redirect) {
                window.location = redirect;
            },
            $$apiErrorHandler: function (error) {
                if (error.data) {
                    $notifier.error(errorUtils.serverErrorToString(error));
                }
            },
            $errorCatch: function (promise, opts) {
                var self = this;
                return $q.when(promise)
                    .catch(function (error) {
                        //Send info to error handler
                        self.$$apiErrorHandler(error);

                        //Continue error propagation to chained promises
                        return $q.reject(error);
                    });
            },
            $successDisplay: function (promise, opts) {
                opts = $.extend({}, opts);
                return $q.when(promise)
                    .then(function (message) {
                        $notifier.success(opts.success || message);
                    });
            },
            reload: function () {
                $window.location.reload();
            },
            scrollToTop: function (opts) {
                $window.scrollTo(0, 0);
            },
            createTimeSet: function (str) {
                var date = $utils.parseTime(str);
                if (date) { //Return null if invalid time given
                    return {
                        date: $utils.parseTime(str),
                        str: str,

                        //Fields added for making objects more searchable
                        str12h: $utils.parseTimeMoment(str).format('h:mm:ss a'),
                        str12hShort: $utils.parseTimeMoment(str).format('ha'),
                        str12hMed: $utils.parseTimeMoment(str).format('h:mm a')
                    };
                }
            },
            createStdTimeSets: function () {
                return $utils.createTimeSets({hourStart: 6, hourEnd: 18});
            },
            createTimeSets: function (opts) {
                var times = [];

                opts = _.assign({
                    hourStart: 0,
                    hourEnd: 24,
                    interval: 30
                }, opts);

                for (var h = opts.hourStart; h < opts.hourEnd; h += 1) {
                    for (var m = 0; m < 60; m += opts.interval) {
                        var hour = h < 10 ? '0' + h : h;
                        var min = m < 10 ? '0' + m : m;

                        var str = hour + ':' + min + ':00';
                        times.push($utils.createTimeSet(str));
                    }
                }
                return times;
            },
            formatPhoneNumber: function (phone) {
                if (phone) {
                    phone = phone.replace(/[^0-9]/g, '');

                    //11 digit number
                    if (phone.length == 11) {
                        phone = phone.replace(/(\d{1})(\d{3})(\d{3})(\d{4})/, '$1($2) $3-$4');
                    }
                    //10 digit number
                    else {
                        phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
                    }

                    return phone;
                }
            },
            parseTimeMoment: function (str) {
                return moment(str, 'HH:mm:ss');
            },
            parseTime: function (str) {
                var m = $utils.parseTimeMoment(str);

                if (m.isValid()) {
                    return m.toDate();
                }
            },
            parseDateMoment: function (str) {
                return moment(str, 'YYYY-MM-DD')
                    .hours(0).minutes(0).seconds(0).milliseconds(0);
            },
            parseDate: function (str) {
                var m = $utils.parseDateMoment(str);

                if (m.isValid()) {
                    return m.toDate();
                }
            },
            dateToJson: function (date) {
                var m = moment(date);

                if (m.isValid()) {
                    return m.format('YYYY-MM-DD');
                }
            },
            dateEquals: function (d1, d2) {
                //Indicates if the date of 2 Date objects are the same
                return $utils.dateToJson(d1) == $utils.dateToJson(d2);
            }
        };

        return $utils;
    });