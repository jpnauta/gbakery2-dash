import { DashGenericUtilsModule } from "./utils";

export const DashGenericModule = angular.module('dash.generic', [
  DashGenericUtilsModule.name,
]);