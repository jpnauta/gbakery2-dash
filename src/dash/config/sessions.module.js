import angular from 'angular';
import { CoreUsersSessionsModule } from "../../core/users/sessions";

export const DashConfigSessionsModule = angular
    .module('dash.config.sessions', [CoreUsersSessionsModule.name])
    .run(($rootScope, userSession) => {
      $rootScope.userSession = userSession;
      userSession.load();
    });