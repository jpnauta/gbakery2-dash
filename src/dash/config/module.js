import angular from 'angular';

import { DashConfigModelsModule } from "./models.module";
import { DashConfigPaginationModule } from "./pagination.module";
import { DashConfigAuthModule } from "./auth.module";
import { DashConfigSessionsModule } from "./sessions.module";
import { CoreGenericFiltersModule } from "../../core/generic/filters";
import { CoreGenericFormsModule } from "../../core/generic/forms";
import { CoreGenericDirectivesModule } from "../../core/generic/directives";
import { CoreConstantsModule } from "../../core/generic/constants";
import { CoreGenericPaginationModule } from "../../core/generic/pagination";
import { CoreGenericSyncModelsModule } from "../../core/generic/sync/models";
import { CoreGenericRepeatersModule } from "../../core/generic/repeaters";
import { CoreConfigResourceModule } from "../../core/config/resource";

export const DashConfigModule = angular
    .module('dash.config', [
      CoreConstantsModule.name, CoreGenericPaginationModule.name, CoreGenericSyncModelsModule.name,
      CoreGenericRepeatersModule.name,

      CoreGenericFiltersModule.name, CoreGenericFormsModule.name, CoreGenericDirectivesModule.name,

      DashConfigModelsModule.name, DashConfigPaginationModule.name, DashConfigAuthModule.name,
      DashConfigSessionsModule.name, CoreConfigResourceModule.name
    ]);
