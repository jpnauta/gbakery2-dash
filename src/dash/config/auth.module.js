import angular from 'angular';
import { CoreConfigAuthorizationJwtModule } from "../../core/config/authorization/jwt";

export const DashConfigAuthModule = angular
    .module('dash.config.auth', [CoreConfigAuthorizationJwtModule.name])
    // Ensure user is logged in
    .run(function ($rootScope, $location, auth, $window, _) {
      $rootScope.$on('$routeChangeStart', function (event) {
        let loginPath = '/users/login';
        let url = $location.url();
        let path = $location.path();

        if (path != loginPath) {
          auth.forceAuth()
              .catch(() => {
                $location.url(loginPath).search('redirect', url);
              });
        }
      });
    });