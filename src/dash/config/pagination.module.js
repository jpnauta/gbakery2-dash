import angular from 'angular';
import { CoreGenericPaginationModule } from "../../core/generic/pagination";

export const DashConfigPaginationModule = angular
    .module('dash.config.pagination', [CoreGenericPaginationModule.name])
    .run($templateCache => {
      //Add default pagination template
      $templateCache.put('core/generic/pagination/paginator.html',
          '<ul class="pagination" ng-if="p.hasMorePages()">' +
          '<li ng-if="p.previous_page_number"><a ng-click="p.goTo(p.previous_page_number)">«</a></li>' +
          '<li ng-repeat="num in p.previous_list"><a ng-click="p.goTo(num)" ng-bind="num"></a></li>' +
          '<li class="active"><a ng-bind="p.page_number"></a></li>' +
          '<li ng-repeat="num in p.next_list"><a ng-click="p.goTo(num)" ng-bind="num"></a></li>' +
          '<li ng-if="p.next_page_number"><a ng-click="p.goTo(p.next_page_number)">»</a></li>' +
          '</ul>'
      );
    })
    .run($templateCache => {
      //Add default pagination template
      $templateCache.put('core/generic/pagination/page-manager-display.html',
          '<div ng-if="manager.page.count > 0" class="page-manager-display clearfix">' +
          '<a class="btn btn-default previous" ng-click="manager.goToPrevious()" ng-disabled="!manager.hasPrevious()">' +
          '«' +
          '</a>' +
          '<div class="info">' +
          'Page [[ manager.page.page_number | number ]]/[[ manager.page.num_pages | number ]]' +
          '</div>' +
          '<a class="btn btn-default next" ng-click="manager.goToNext()" ng-disabled="!manager.hasNext()">' +
          '»' +
          '</a>' +
          '</div>'
      );
    });
