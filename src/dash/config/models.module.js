import angular from 'angular';
import { CoreGenericModelsModule } from "../../core/generic/models";
import { CoreExtConfigModule } from "../../core/ext/config";

export const DashConfigModelsModule = angular
    .module('dash.config.models', [CoreGenericModelsModule.name, CoreExtConfigModule.name])
    .config((ModelFactoryProvider, CONFIG) => {
      ModelFactoryProvider.setDomainUrl(CONFIG.app.url);
    });