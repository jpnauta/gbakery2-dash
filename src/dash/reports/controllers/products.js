/* @ngInject */
export function ProductsReportController($scope, $q, $utils, $constants, $notifier, LoaderFactory,
          FrontProduct, ProductCategory, SpecialProduct, _, moment, OrderProduct) {
  var load = LoaderFactory.errorOnly();

  $scope.date = moment().add(1, 'days').hours(0).toDate();
  $scope.MAX_PRODUCTS_BY_ORDER = 3;

  $scope.dateSelected = function () {
    return Boolean($scope.date);
  };

  $scope.$loadCategoryReport = function () {
    const params = {date: moment($scope.date).format('YYYY-MM-DD')};
    return ProductCategory.report(params).$promise.then(function (categories) {
      $scope.categories = categories;
    });
  };

  $scope.$loadSpecialProducts = function () {
    const params = {order__date: moment($scope.date).format('YYYY-MM-DD'), product_type: 'special_product'};
    return OrderProduct.stats(params).$promise.then(function (specialProducts) {
      $scope.specialProductStats = specialProducts;
    });
  };

  $scope.$loadAll = function () {
    return load($q.all([
      $scope.$loadCategoryReport(),
      $scope.$loadSpecialProducts()
    ]));
  };

  //Load reports when
  $scope.$watch('date', function () {
    if ($scope.date) {
      $scope.$loadAll();
    }
  });

  $scope.$quantityType = function (quantity_type) {
    return $constants.QUANTITY_TYPES_BY_ID[quantity_type];
  };

  $scope.$productQuantity = function (quantity, type) {
    return quantity / $scope.$quantityType(type).quantity;
  };

  $scope.printReport = function () {
    window.print();
  };

  //Hide front products received that have 0 quantity
  $scope.$watch('categories', function (categories) {
    _.each(categories, function (category) {
      category.$frontProducts = _.filter(category.all_front_products, function (fp) {
        return fp.quantity > 0;
      });
    });
  });

  //Hide baker products received that have 0 quantity
  $scope.$watch('categories', function (categories) {
    _.each(categories, function (category) {
      category.$bakerProducts = _.filter(category.all_baker_products, function (bp) {
        return bp.quantity > 0;
      });
    });
  });

  $scope.$quantityType = function (quantity_type) {
    return $constants.QUANTITY_TYPES_BY_ID[quantity_type];
  };

  $scope.$productQuantity = function (quantity, type) {
    return quantity / $scope.$quantityType(type).quantity;
  };
}