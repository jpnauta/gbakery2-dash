/* @ngInject */
export function OrdersReportController($scope, $q, $utils, LoaderFactory, Order, OrderFlag, $orderUtils, _, moment, $constants) {
    const load = LoaderFactory.errorOnly();
    const bgLoad = LoaderFactory.errorOnly({property: '$bgLoading'});

    $scope.date = moment().add(1, 'days').toDate();

    $scope.PRINT_METHOD_OPTIONS = $constants.PRINT_METHOD_OPTIONS;
    $scope.PRINT_METHOD_LIST = $constants.PRINT_METHOD_LIST;
    $scope.printMethod = $scope.PRINT_METHOD_OPTIONS["PRINT_THERMAL"];

    $orderUtils.orderActions($scope, bgLoad);
    $orderUtils.liveOrderLoader($scope, load, bgLoad, {
        picked_up: false,
        cancelled: false,
    });
    $orderUtils.printCommon($scope);

    $scope.printReport = function () {
        window.print();
    };

    $scope.$watch("orders", () => {
        $scope.$updateOrderSlipMap($scope.orders);
    });

    $scope.togglePickedUpOrders = function () {
        $scope.$togglePropertyState($scope.params, 'picked_up', false, null);
    };

    $scope.toggleCancelledOrders = function () {
        $scope.$togglePropertyState($scope.params, 'cancelled', false, null);
    };
}

/* @ngInject */
export function orderReportFilter(_, $filter) {
    return function (list, params) {
        //Filter orders with the specified order flag, if specified
        if (params.orderFlags) {
            list = _.filter(list, function (order) {
                return _.find(order.order_flags, function (flagID) {
                    return _.contains(params.orderFlags, flagID);
                });
            });
        }

        //Filter orders with the specified order flag, if specified
        if (params.orderFlagsOmit) {
            list = _.filter(list, function (order) {
                return !_.find(order.order_flags, function (flagID) {
                    return _.contains(params.orderFlagsOmit, flagID);
                });
            });
        }

        //Filter by picked up, if specified
        if (_.isBoolean(params.picked_up)) {
            list = $filter('filter')(list, {picked_up: params.picked_up});
        }

        //Filter by cancelled, if specified
        if (_.isBoolean(params.cancelled)) {
            list = $filter('filter')(list, {cancelled: params.cancelled});
        }

        return list;
    };
}