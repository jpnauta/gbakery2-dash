import { DashProductsModule } from "../products/module";
import { DashGenericModule } from "../generic";
import { DashOrdersModule } from "../orders";
import {orderReportFilter, OrdersReportController} from "./controllers/orders";
import { ProductsReportController } from "./controllers/products";
import { DashUsersLoginModule } from "../users/login";
import { DashUsersLogoutModule } from "../users/logout";

export const DashReportsModule = angular
    .module('dash.reports', [
      DashGenericModule.name, DashOrdersModule.name, DashProductsModule.name,
      DashUsersLoginModule.name, DashUsersLogoutModule.name
    ])
    .controller('OrdersReportController', OrdersReportController)
    .filter('orderReportFilter', orderReportFilter)
    .controller('ProductsReportController', ProductsReportController);