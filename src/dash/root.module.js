import {CoreAnimationModule} from "../core/generic/animation";
import ngResource from "angular-resource";
import ngCookies from "angular-cookies";
import ngSanitize from "angular-sanitize";
import ngAnimate from "angular-animate";
import ngMaterial from "angular-material";
import ngRoute from "angular-route";
import ngUiMask from "angular-ui-mask";
import ngInfiniteScroll from "ng-infinite-scroll";
import {CoreComponentsPageTitleModule} from "../core/components/page-title";
import {CoreGenericExtensionsModule} from "../core/generic/extensions";
import ngCookie from "angular-cookie";
import {DashRoutesModules} from "./routes";
import {DashConfigModule} from "./config/module";
import {DashProductsModule} from "./products/module";
import {DashGenericModule} from "./generic";
import {DashOrdersModule} from "./orders";
import {DashBocfModule} from "./bocf";
import {DashReportsModule} from "./reports";
import $ from 'jquery';

import './root.module.scss';

angular.module('dApp', [
    //libraries
    ngCookies,
    ngResource,
    ngSanitize,
    ngAnimate,
    ngMaterial,
    ngRoute,
    ngUiMask,
    ngInfiniteScroll,

    ngCookie,

    CoreComponentsPageTitleModule.name,
    DashRoutesModules.name,
    DashConfigModule.name,
    CoreAnimationModule.name,
    DashProductsModule.name,
    DashGenericModule.name,
    DashOrdersModule.name,
    DashReportsModule.name,
    CoreGenericExtensionsModule.name,
    DashBocfModule.name,
])
    .config(function ($httpProvider, $interpolateProvider) {
        //better approach in terms of testability
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';

        //Retrieve CSRF token from page & set it
        let csrfToken = angular.element(document).find('[name="csrfmiddlewaretoken"]').attr('value');
        $httpProvider.defaults.headers.common['X-CSRFToken'] = csrfToken;

        // Change start and end to work with Django
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }).run(function ($rootScope, $mdSidenav) {
    $rootScope.toggleSidebar = function () {
        $mdSidenav('left').toggle();
    };
    $rootScope.closeSidebar = function () {
        $mdSidenav('left').close();
    };
})
    .config(function ($mdDateLocaleProvider, moment) {
        $mdDateLocaleProvider.formatDate = function (date) {
            return moment(date).format('YYYY-MM-DD');
        };
    })
    .config(function ($locationProvider) {
        $locationProvider.html5Mode(true)
    })
    // Disable unhandled rejection warnings
    .config(function ($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    });


// Disable aria warnings from angular-material
(((console) => {
    try {
        /*eslint no-console: "off"*/
        var oldWarn = console.warn;
        console.warn = function (arg1) {
            if (arg1 && arg1.substring(0, 5) == 'ARIA:') {
                return;
            }
            oldWarn.apply(console, arguments);
        };
        console.warn('ARIA warnings disabled.');
    } catch (e) { // If failed, ignore

    }
}))(window.console);

// Hide loader after everything is loaded
$(() => {
    for (let element of document.getElementsByClassName('loader')) {
        element.style.display = 'none';
    }
});
