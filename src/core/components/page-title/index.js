import { PageTitle } from "./page-title";

export const CoreComponentsPageTitleModule = angular.module('core.components.page-title', ['core.ext.lodash'])
    .directive("pageTitle", PageTitle);
