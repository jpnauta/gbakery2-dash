import { CoreGenericModelsModule } from "../generic/models";
import { CoreConfigResourceModule } from "./resource";
import { CoreGenericObjectsModule } from "../generic/objects";

angular
    .module('core.config.models', [CoreGenericModelsModule.name, CoreConfigResourceModule.name, CoreGenericObjectsModule.name])
    .config(($httpProvider, objectUtils, _) => {
        // Don't send private variables to server
        $httpProvider.interceptors.push(() => {
            return {
                request: function (config) {
                    var omitPrivateVariables = !('omitPrivateVariables' in config) || config.omitPrivateVariables;

                    if (omitPrivateVariables) {
                        // Filter out private variables
                        config.data = objectUtils.omitPrivateVariables(_.cloneDeep(config.data));
                        config.params = objectUtils.omitPrivateVariables(_.cloneDeep(config.params));
                    }


                    return config;
                }
            };
        });
    });
