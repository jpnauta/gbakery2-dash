import ngResource from 'angular-resource';

export const CoreConfigResourceModule = angular.module('core.config.resource', [ngResource])
    .config($resourceProvider => {
      //Don't strip trailing slashes - required for Django
      $resourceProvider.defaults.stripTrailingSlashes = false;
    });
