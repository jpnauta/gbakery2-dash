import { CoreUsersSessionsModule } from "../users/sessions";

export const CoreConfigStorageModule = angular.module('core.config.storage', [CoreUsersSessionsModule.name])
    .config(localStorageServiceProvider => {  // Configure LocalStorageModule
      localStorageServiceProvider
          .setPrefix('gbakery2')
          .setStorageType('localStorage');

      if (localStorageServiceProvider.isSupported) {  //Use session storage if local storage doesn't work
        localStorageServiceProvider
            .setStorageType('sessionStorage');
      }
    })
    .config((userCacheStorageProvider, $$cookieReaderProvider, $httpProvider) => {  // Configure userCacheStorage
      var key = 'gbakery2-login-id',
          cookieReader = $$cookieReaderProvider.$get(angular.element(document))();

      function randomString() {
        return String(Math.floor(Math.random() * 1000) + 1);
      }

      // Attempt to parse session UUID from cookies and pass it to userCacheStorage
      var sessionKey = cookieReader[key];

      // If session key can't be parsed from document, use random string for now
      if (!sessionKey) {
        sessionKey = randomString();
      }

      userCacheStorageProvider.setSessionKey(sessionKey);

      // Update session key whenever response is received
      $httpProvider.interceptors.push(() => ({
        response: function (response) {
          // Check if login ID is set; if it is, update user session cache
          var sessionID = cookieReader[key];
          if (sessionID) {
            userCacheStorageProvider.setSessionKey(sessionID);
          }

          return response;
        }
      }));
    })
    .run(userCacheStorage => {
      // Clear old session data from cache
      userCacheStorage.clearOldSessions();
    });