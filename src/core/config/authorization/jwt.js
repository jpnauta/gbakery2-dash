import ngCookies from "angular-cookies";
import { CoreAccountsAuthorizationJwtModule } from "../../accounts/authorization/jwt";
import { CoreConfigAuthorizationModule } from "./index";
import { CoreUsersSessionsModule } from "../../users/sessions";

/**
 * Configures the app to use JWT for all requests. See `core.accounts.authorization` for more info
 */
export const CoreConfigAuthorizationJwtModule = angular
    .module('core.config.authorization.jwt', [CoreConfigAuthorizationModule.name, CoreAccountsAuthorizationJwtModule.name, CoreUsersSessionsModule.name, ngCookies])
    .run((auth, userSession, $cookies, JWTClient) => {
      // Tell auth system to use JWT client
      auth.setClient(new JWTClient());

      // Delete Django's default cookies
      $cookies.remove('_ga');
      $cookies.remove('_gat');
    });