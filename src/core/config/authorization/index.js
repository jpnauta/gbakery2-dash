import { CoreConfigStorageModule } from "../storage";
import { CoreAccountsAuthorizationModule } from "../../accounts/authorization";

export const CoreConfigAuthorizationModule = angular.module('core.config.authorization', [CoreAccountsAuthorizationModule.name, CoreConfigStorageModule.name])
    .config(($httpProvider, authProvider) => {
      // Configure interceptor to apply the authorization info of `jwtAuth` to `ngResource` API calls
      $httpProvider.interceptors.push(() => ({
        request: authProvider.httpInterceptor
      }));
    });