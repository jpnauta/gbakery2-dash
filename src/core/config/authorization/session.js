/**
 * Configures the app to use session for all requests. See `core.accounts.authorization` for more info
 */
import { CoreConfigAuthorizationModule } from "./index";
import { CoreAccountsAuthorizationSession } from "../../accounts/authorization/session";

export const CoreConfigAuthorizationJwtModule = angular
    .module('core.config.authorization.session', [CoreConfigAuthorizationModule.name, CoreAccountsAuthorizationSession.n])
    .run((auth, SessionClient) => {
      // Tell auth system to use session client
      auth.setClient(new SessionClient());
    });