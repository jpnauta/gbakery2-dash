import { CoreGenericModelsModule } from "../generic/models";

export const CoreAccountModelsModule = angular.module('core.accounts.models', [CoreGenericModelsModule.name])
    .factory('Auth', ModelFactory => ModelFactory({
        baseUrl: '/api/auth/',
        actions: {
            login: {
                method: 'POST',
                params: {
                    listCtrl: 'login'
                }
            },
            logout: {
                method: 'POST',
                params: {
                    listCtrl: 'logout'
                }
            },
            register: {
                method: 'POST',
                params: {
                    listCtrl: 'registration'
                }
            },
            passwordReset: {
                method: 'POST',
                params: {
                    listCtrl: 'password',
                    docCtrl: 'reset'
                }
            },
            passwordChange: {
                method: 'POST',
                params: {
                    listCtrl: 'password',
                    docCtrl: 'change'
                }
            },
            facebook: {
                method: 'POST',
                params: {
                    listCtrl: 'facebook'
                }
            }
        }
    }))
    .factory('AuthJWT', ModelFactory => ModelFactory({
        baseUrl: '/api/auth/',
        actions: {
            login: {
                method: 'POST',
                params: {
                    listCtrl: 'login'
                }
            },
            logout: {
                method: 'POST',
                params: {
                    listCtrl: 'logout'
                }
            },
            refresh: {
                method: 'POST',
                params: {
                    listCtrl: 'token',
                    docCtrl: 'refresh'
                }
            }
        }
    }))
    .factory('AuthProfile', ModelFactory => ModelFactory({
        baseUrl: '/api/auth/profile/',
        actions: {
            emailChange: {
                method: 'POST',
                params: {
                    listCtrl: 'email',
                    docCtrl: 'change'
                }
            }
        }
    }));
