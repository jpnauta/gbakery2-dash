import {CoreExtLodashModule} from "../../ext/lodash";
import {CoreExtMomentModule} from "../../ext/moment";
import {CoreAccountsAuthorizationModule} from "./index";
import {CoreUsersSessionsModule} from "../../users/sessions";
import {CoreAccountModelsModule} from "../models";

export const CoreAccountsAuthorizationJwtModule = angular
    .module('core.accounts.authorization.jwt', [CoreAccountsAuthorizationModule.name, CoreAccountModelsModule.name, CoreUsersSessionsModule.name, CoreExtMomentModule.name, CoreExtLodashModule.name])
    /**
     * Auth client that uses JWT authentication
     */
    .service('JWTClient', (moment, _, $log, AuthJWT, Auth, cacheStorage, $q) => function () {
      var scope = this;

      scope.opts = {
        tokenKey: 'jwtClientToken',
        refreshTokenKey: 'jwtClientRefreshToken',
        tokenExpires: 6,  // Hours
        tokenRefreshExpires: 30 * 24 // Hours
      };

      scope.changeListeners = [];

      var authToken = null;  // Auth token of the user currently logged in
      var authRefreshToken = null;  // Auth refresh token of the user currently logged in

      /**
       * Retrieves the auth token of the user currently logged in
       * @returns {*}
       */
      function getAuthToken() {
        return authToken;
      }

      /**
       * Retrieves the auth refresh token of the user currently logged in
       * @returns {*}
       */
      function getAuthRefreshToken() {
        return authRefreshToken;
      }

      /**
       * @returns {*} The auth token to be added to request
       */
      function getAuthHeader(useAuth) {
        if (useAuth && authToken) {
          return 'Bearer ' + authToken;
        } else {
          // Force anonymous authentication
          return 'Anonymous';
        }
      }

      /**
       * Applies auth token to Angular resource calls
       */
      function httpInterceptor(config) {
        var useAuth = !('useAuth' in config) || config.useAuth;
        config.headers['Authorization'] = getAuthHeader(useAuth);

        return config;
      }


      // Recall auth token from cache
      authToken = cacheStorage.get(scope.opts.tokenKey);
      authRefreshToken = cacheStorage.get(scope.opts.refreshTokenKey);

      function setAuthToken(token, refreshToken = undefined) {
        authToken = token;

        // Store token in cache
        cacheStorage.set(scope.opts.tokenKey, token, moment().add(scope.opts.tokenExpires, 'hours').toDate());

        // Store refresh token, if given
        if (refreshToken) {
          authRefreshToken = refreshToken;
          cacheStorage.set(scope.opts.refreshTokenKey, refreshToken, moment().add(scope.opts.tokenRefreshExpires, 'hours').toDate());
        }

        // Trigger change listeners
        _.each(scope.changeListeners, listener => {
          listener();
        });
      }

      /**
       * Performs a JWT token refresh
       * @returns {Promise} promise containing new auth token, or null if none unsuccessful
       */
      function $refreshAuthToken(token) {
        return new AuthJWT({ refresh: token }).$refresh()
            .then(data => // Refresh successful
                data.access)
            .catch(() => {  // Refresh failed
              $log.error('Could not refresh JWT token: ' + token);
              return null;
            });
      }

      /**
       * Checks whether the user is logged in, refreshing auth information as needed.
       * Should be called when
       *
       * WARNING: does not account for invalid tokens, server issues, and the like. If
       * you need this, use `forceAuth` instead
       *
       * @return {Promise} promise indicating if the user is successfully logged in
       */
      function checkAuth() {
        var token = getAuthToken();
        var refreshToken = getAuthRefreshToken();

        if (refreshToken) {  // Token currently set
          if (token) {  // No JWT refresh is needed
            return $q.when(null);
          } else {  // JWT refresh needed
            return $refreshAuthToken(refreshToken)
                .then(newToken => {
                  setAuthToken(newToken);
                  if (!newToken) {
                    return $q.reject(newToken);
                  }
                });
          }
        } else {  // No token found
          return $q.reject(null);  // User is not logged in
        }
      }

      /**
       * Force refreshes the authorization. If this function is completed successfully,
       * we should be 100% confident that the client is authorized with the server.
       *
       * @return {Promise} promise indicating if the user is successfully logged in
       */
      function forceAuth() {
        var refreshToken = getAuthRefreshToken();

        if (refreshToken) {  // Token currently set
          return $refreshAuthToken(refreshToken)
              .then(newToken => {
                setAuthToken(newToken);
                if (!newToken) {
                  return $q.reject(newToken);
                }
              });
        } else {  // No token found
          return $q.reject(null);  // User is not logged in
        }
      }

      /**
       * Attempts to login the user. If the login is successful, the auth token.
       *
       * @param email user's email
       * @param password user's password
       *
       * @return {Promise} Promise triggered when login is complete (or failed)
       */
      function login(email, password) {
        return new AuthJWT({ email, password }).$login()
            .then(data => {
              setAuthToken(data.access_token, data.refresh_token);
              return data;
            })
            .catch(data => {
              setAuthToken(null);
              return $q.reject(data);
            });
      }

      /**
       * Attempts to register a new user. If successful, the auth token is stored.
       *
       * @param data data sent to server. See Auth.register for more info.
       *
       * @return {Promise} Promise triggered when registration is complete (or failed)
       */
      function register(data) {
        return new Auth(data).$register()
            .then(data => {
              setAuthToken(data.token);
              return data;
            })
            .catch(data => {
              setAuthToken(null);
              return $q.reject(data);
            });
      }

      /**
       * Logs the user out
       *
       * @return {Promise} Promise containing Angular Resource response data
       */
      function logout() {
        return new Auth().$logout()  //Attempt to tell servers that user is logging out
            .finally(() => {
              // Regardless of success or fail, delete token locally
              setAuthToken(null);
            });
      }

      /**
       * Attempts to login the user via Facebook, storing auth credentials as needed.
       *
       * @param token Facebook auth token
       *
       * @return {Promise} Promise triggered when login is complete (or failed)
       */
      function facebookLogin(token) {
        return new Auth({ access_token: token, useAuth: false }).$facebook()
            .then(data => {
              setAuthToken(data.token);
              return data;
            })
            .catch(data => {
              setAuthToken(null);
              return $q.reject(data);
            });
      }

      // Public methods
      scope.login = login;
      scope.logout = logout;
      scope.checkAuth = checkAuth;
      scope.forceAuth = forceAuth;
      scope.register = register;
      scope.httpInterceptor = httpInterceptor;
      scope.facebookLogin = facebookLogin;

      //Private methods
      scope.$getAuthToken = getAuthToken;
      scope.$setAuthToken = setAuthToken;
    });