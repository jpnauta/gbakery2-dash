import { CoreExtLodashModule } from "../../ext/lodash";

export const CoreAccountsAuthorizationModule = angular.module('core.accounts.authorization', [CoreExtLodashModule.name])
    .provider('auth', function (_) {
        var client = null;  // Client currently being used
        var changeListeners = [];

        this.httpInterceptor = config => // Trigger change listeners
            client.httpInterceptor(config);

        this.$get = function ($log) {
            var scope = this;

            scope.$checkClient = () => {
                if (!client) {
                    $log.error('Auth client not set');
                }
            };

            scope.$indicateAuthChange = () => {
                _.each(scope.changeListeners, listener => {
                    listener();
                });
            };

            /**
             * Attempts to login the user, storing auth credentials as needed.
             *
             * @param email user's email
             * @param password user's password
             *
             * @return {Promise} Promise triggered when login is complete (or failed)
             */
            scope.login = (email, password) => {
                scope.$checkClient();
                return client.login(email, password).finally(scope.$indicateAuthChange);
            };

            /**
             * Logs the user out
             *
             * @return {Promise} Promise triggered after logout is complete
             */
            scope.logout = () => {
                scope.$checkClient();
                return client.logout().finally(scope.$indicateAuthChange);
            };

            /**
             * Checks whether the user is logged in, refreshing auth information as needed.
             *
             * WARNING: does not account for invalid tokens, server issues, and the like. If
             * you need this, use `forceAuth` instead
             *
             * @return {Promise} promise indicating if the user is successfully logged in
             */
            scope.checkAuth = () => {
                scope.$checkClient();
                return client.checkAuth();
            };

            /**
             * Force refreshes the authorization. If this function is completed successfully,
             * we should be 100% confident that the client is authorized with the server.
             *
             * @return {Promise} promise indicating if the user is successfully logged in
             */
            scope.forceAuth = () => {
                scope.$checkClient();
                return client.forceAuth();
            };

            /**
             * Attempts to register a new user. If successful, the auth token is stored.
             *
             * @param data data sent to server. See Auth.register for more info.
             *
             * @return {Promise} Promise triggered when registration is complete (or failed)
             */
            scope.register = data => {
                scope.$checkClient();
                return client.register(data).finally(scope.$indicateAuthChange);
            };

            /**
             * Indicates with auth client to use for authentication
             * @param newClient client to use
             */
            scope.setClient = newClient => {
                client = newClient;
                scope.$indicateAuthChange();
            };

            /**
             * Get auth client currently being used
             *
             * @return {Object} Client object currently being used
             */
            scope.getClient = () => {
                return client;
            };

            /**
             * Attempts to login the user via Facebook, storing auth credentials as needed.
             *
             * @param token Facebook auth token
             * @param email email address given from Facebook
             *
             * @return {Promise} Promise triggered when login is complete (or failed)
             */
            scope.facebookLogin = token => {
                scope.$checkClient();
                return client.facebookLogin(token).finally(scope.$indicateAuthChange);
            };

            scope.changeListeners = changeListeners;

            return scope;
        };
    });