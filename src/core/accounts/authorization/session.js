import { CoreExtLodashModule } from "../../ext/lodash";
import { CoreExtMomentModule } from "../../ext/moment";
import { CoreAccountsAuthorizationModule } from "./index";
import { CoreAccountModelsModule } from "../models";

export const CoreAccountsAuthorizationSession = angular
    .module('core.accounts.authorization.session', [CoreAccountsAuthorizationModule.name, CoreAccountModelsModule.name, CoreExtMomentModule.name, CoreExtLodashModule.name])
/**
 * Auth client that uses session authentication
 */
    .service('SessionClient', (moment, _, $log, Auth) => function () {
        var scope = this;

        scope.opts = {};

        /**
         * Blank HTTP interceptor
         */
        function httpInterceptor(config) {
            return config;
        }

        /**
         * Checks whether the user is logged in, refreshing auth information as needed.
         *
         * WARNING: does not account for invalid tokens, server issues, and the like. If
         * you need this, use `forceAuth` instead
         *
         * @return {Promise} promise indicating if the user is successfully logged in
         */
        function checkAuth() {
            throw new Error('Not implemented yet');
        }

        /**
         * Force refreshes the authorization. If this function is completed successfully,
         * we should be 100% confident that the client is authorized with the server.
         *
         * @return {Promise} promise indicating if the user is successfully logged in
         */
        function forceAuth() {
            throw new Error('Not implemented yet');
        }

        /**
         * Attempts to login the user. If the login is successful, the auth token.
         *
         * @param email user's email
         * @param password user's password
         *
         * @return {Promise} Promise triggered when login is complete (or failed)
         */
        function login(email, password) {
            return new Auth({email, password}).$login();
        }

        /**
         * Attempts to register a new user. If successful, the auth token is stored.
         *
         * @param data data sent to server. See Auth.register for more info.
         *
         * @return {Promise} Promise triggered when registration is complete (or failed)
         */
        function register(data) {
            return new Auth(data).$register();
        }

        /**
         * Logs the user out
         *
         * @return {Promise} Promise containing Angular Resource response data
         */
        function logout() {
            return new Auth().$logout();  //Attempt to tell servers that user is logging out
        }

        /**
         * Attempts to login the user via Facebook, storing auth credentials as needed.
         *
         * @param token Facebook auth token
         *
         * @return {Promise} Promise triggered when login is complete (or failed)
         */
        function facebookLogin(token) {
            return new Auth({access_token: token}).$facebook();
        }

        // Public methods
        scope.login = login;
        scope.logout = logout;
        scope.checkAuth = checkAuth;
        scope.forceAuth = forceAuth;
        scope.register = register;
        scope.httpInterceptor = httpInterceptor;
        scope.facebookLogin = facebookLogin;
    });