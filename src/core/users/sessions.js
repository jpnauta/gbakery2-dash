import { CoreExtMomentModule } from "../ext/moment";
import { CoreAccountsAuthorizationModule } from "../accounts/authorization";
import { CoreAccountModelsModule } from "../accounts/models";
import { CoreGenericStorageModule } from "../generic/storage";

export const CoreUsersSessionsModule = angular
    .module('core.users.sessions', [CoreGenericStorageModule.name, CoreAccountModelsModule.name, CoreExtMomentModule.name, CoreAccountsAuthorizationModule.name])
    .service('userSession', function (userCacheStorage, AuthProfile, $q, $log, moment, auth) {
      var self = this;

      self.opts = {
        baseKey: 'userSession',
        maxAge: 120 // in seconds
      };

      self.$loaded = true;
      self.data = userCacheStorage.get(self.opts.baseKey);

      self.$loadSessionData = (forceLoad) => {
        self.$loaded = false;

        let promise;
        if (forceLoad || !self.data) {  // Cache miss; load info from server
            promise = AuthProfile.get().$promise
                .then(profile => {
                    const data = {
                        loggedIn: true,
                        profile
                    };

                    userCacheStorage.set(self.opts.baseKey, data, moment().add(self.opts.maxAge, 'second'));

                    return data;
                })
                .catch(response => {
                    if (response.status == 401) { // User is not logged in
                        return {
                            loggedIn: false
                        };
                    }
                    $log.error('Could not load venue session info: ', response);
                    return $q.reject(response);
                })
        }
        else {
            promise = $q.when(self.data);
        }

          return promise.then(data => {
              self.$loaded = true;
              self.data = data;
              return data;
          });
      };

      /**
       * Force loads the user's venue session data
       * @return {Promise} promise triggered when session is re-loaded
       */
      self.reload = () => self.$loadSessionData(true);

      /**
       * Loads the user's venue session data, if it hasn't been loaded already
       * @return {Promise} promise triggered when session is loaded
       */
      self.load = () => self.$loadSessionData(false);

      /**
       * Clears any existing data loaded
       */
      self.clear = () => {
        self.data = null;
        userCacheStorage.remove(self.opts.baseKey);
      };
    });