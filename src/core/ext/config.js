import { CONFIG } from "../../dash/config";

export const CoreExtConfigModule = angular.module('core.ext.config', [])
    .constant('CONFIG', CONFIG);

