const p = Array.prototype;

p.remove = function (item) {
  var index = this.indexOf(item);
  if (index > -1) {
    this.splice(index, 1);
  }
};

p.isEmpty = function () {
  return this.length == 0;
};

export const CoreExtArrayModule = angular.module('core.ext.array', []);