import { CoreExtStringsModule } from "./string";
import { CoreExtArrayModule } from "./array";
import { CoreExtLodashModule } from "./lodash";
import { CoreExtMomentModule } from "./moment";

export const CoreExtModule = angular.module('core.ext', [
  CoreExtLodashModule.name, CoreExtMomentModule.name,
  CoreExtStringsModule.name, CoreExtArrayModule.name
]);
