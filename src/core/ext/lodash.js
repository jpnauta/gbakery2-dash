const _ = window._ = require('lodash');

// Basic shims for reverse compatibility
if (typeof _.contains === 'undefined') {
  _.mixin({ 'contains': _.includes });
}
if (typeof _.object === 'undefined') {
  _.mixin({ 'object': _.zipObject });
}
if (typeof _.pluck === 'undefined') {
  _.mixin({ 'pluck': _.map });
}
if (typeof _.indexBy === 'undefined') {
  _.mixin({ 'indexBy': _.keyBy });
}

function toBoolean(value) {
  if (!value) {
    return false;
  }
  if (typeof value == 'number' || typeof value == 'boolean') {
    return !!value;
  }
  return _.replace(_.trim(value.toLowerCase()), /[""'']/ig, '') === 'true';
}

const withIndex = (fn) => {
  let index = 0
  return (thing) => fn(thing, index++)
}

/**
 * Similar to `_.chunk`, but instead of creating a small
 * chunk at the end, the chunks are divided equally as best
 * as possible.
 *
 * @param {Array} array The array to process.
 * @param {number} maxCount The length of each chunk
 * @returns {Array} Returns the new array of chunks.
 */
function chunkEvenly(array, maxCount) {
  const count = array.length;
  return _(array)
      .groupBy(withIndex((element, index) => {
        const requiredChunks = Math.ceil(count / maxCount);
        const divisor = count / requiredChunks;
        return Math.floor(index / divisor);
      }))
      .map()
      .value();
}

_.mixin({
  'toBoolean': toBoolean,
  'chunkEvenly': chunkEvenly,
});

export const CoreExtLodashModule = angular.module('core.ext.lodash', [])
    .constant('_', window._);

