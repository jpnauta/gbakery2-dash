import * as moment from 'moment';

export const CoreExtMomentModule = angular.module('core.ext.moment', [])
    .constant('moment', moment);

