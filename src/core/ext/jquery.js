import $ from 'jquery';

export const CoreExtJquery = angular.module('core.ext.jquery', [])
    .constant('$', $);

