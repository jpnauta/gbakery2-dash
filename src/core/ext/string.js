const p = String.prototype;

p.startsWith = function (str) {
  return this.indexOf(str) == 0;
};


export const CoreExtStringsModule = angular.module('core.ext.strings', []);
