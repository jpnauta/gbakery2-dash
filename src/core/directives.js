(function () {
    angular.module('core.directives', [])

        .service('$events', function () {
            var $scope = this;

            $scope.KEYS = {
                TAB: 9,
                ENTER: 13
            };

            $scope.$keydownListener = function (scope, element, key, expr) {
                element.bind('keydown', function (event) {
                    if (event.which === key) {
                        var prevent = scope.$eval(expr, {'$event': event});

                        if (prevent) {
                            event.preventDefault();
                        }
                    }
                });
            };
        })
        .directive('btnLoading', function () {
            return {
                scope: {
                    btnLoading: '&',
                    btnLoadingText: '@',
                    btnLoadingIcon: '@'
                },
                template: '<span>' +
                '<span ng-if="!btnLoading()">' +
                '<i ng-class="btnLoadingIcon"></i>' +
                '<span ng-bind="btnLoadingText"></span>' +
                '</span>' +
                '<span ng-if="btnLoading()">' +
                '<i class="icon-spin icon-spinner"></i> ' +
                '<span>Loading</span>' +
                '</span>' +
                '</span>'

            };
        })
        .directive('caretUpDown', function () {
            return {
                scope: {
                    'caretUpDown': '&'
                },
                template: '<i class="fa" ng-class="$klass"></i>',
                link: function (scope, element, attrs) {
                    var opts = {
                        upClass: 'fa-angle-up',
                        downClass: 'fa-angle-down'
                    };

                    scope.$watch(scope.caretUpDown, function (val) {
                        if (val) {
                            scope.$klass = opts.downClass;
                        }
                        else {
                            scope.$klass = opts.upClass;
                        }
                    });
                }
            };
        })
        .directive('clearButton', function () {
            return {
                scope: {
                    clearButton: '=',
                    klass: '@clearButtonClass'
                },
                template: '<div class="search-clear" ng-class="klass" ng-show="clearButton" ng-click="clearButton = null">' +
                '<i class="fa fa-times-circle"></i>' +
                '</div>'
            };
        })
        .directive('focusMe', function ($timeout, $parse, $rootScope) {
            return {
                link: function (scope, element, attrs) {
                    var model = $parse(attrs.focusMe);

                    //Focus on element when value is set to true
                    scope.$watch(model, function (value) {
                        if (value === true) {
                            $timeout(function () {
                                element[0].focus();
                            });
                        }
                    });

                    //Revert model value on blur
                    element.bind('blur', function () {
                        $timeout(function () {
                            model.assign(scope, false);
                        });
                    });
                }
            };
        })
        .directive('mdSelectedItemRequired', function () {
            return {
                link: function (scope, element, attrs) {
                    scope.$watch(attrs.mdSelectedItemRequired, function (required) {
                        required = Boolean(required);

                        if (required) {
                            attrs.$set('required', 'required');
                        }
                        else {
                            element.removeAttr('required');
                        }
                    });
                }
            };
        })
        .directive('noFocusDatetimepickerButtons', function ($timeout) {
            return {
                link: function (scope, element, attrs) {
                    scope.$setButtonTabIndexes = function () {
                        //Find datepicker associated with input and make them unfocusable
                        element.next().find('[datepicker]').find('button').attr('tabIndex', -1);
                    };

                    $timeout(scope.$setButtonTabIndexes); //Set tab indexes once directive adds datepicker
                    element.focus(scope.$setButtonTabIndexes); //Fixes when input is hidden and then shown again
                }
            };
        })
        .directive('readMore', ['$log', function ($log, _) {
            return {
                restrict: 'A',
                replace: true,
                template: '<span>' +
                '<span>' +
                '<span ng-show="moreText" ng-bind="moreText"></span>' +
                '<span ng-show="overflow && !readMore" ng-click="showMore()"> ' +
                '<span ng-bind="ellipsis"></span> ' +
                '<a class="read-more" ng-bind="moreMessage"></a>' +
                '</span>' +
                '</span>' +
                '<span ng-if="overflow && readMore">' +
                '<span ng-show="lessText" ng-bind="lessText"></span> ' +
                '<a class="read-more" ng-click="showLess()" ng-bind="lessMessage"></a>' +
                '</span>' +
                '</span>',
                scope: {
                    moreMessage: '=?',
                    lessMessage: '=?',
                    countBy: '=?',
                    ellipsis: '=?',
                    limit: '=?',
                    content: '@'
                },
                link: function (scope, elem, attr, ctrl) {
                    //Set default values
                    _.defaults(scope, {
                        moreMessage: 'More',
                        lessMessage: 'Less',
                        limit: 150,
                        ellipsis: '...',
                        countBy: 'chars'
                    });

                    //Split text into moreText and lessText
                    attr.$observe('content', function (text) {
                        text = text || '';

                        if (scope.countBy == 'words') { // Count words
                            var foundWords = text.split(/\s+/);

                            scope.overflow = foundWords.length > scope.limit;
                            scope.moreText = foundWords.slice(0, scope.limit).join(' ');
                            scope.lessText = ' ' + foundWords.slice(scope.limit, foundWords.length).join(' ');

                        } else { // Count characters
                            if (scope.countBy != 'chars') {
                                $log.warn("unexpected count method: '" + scope.countBy + "'");
                            }

                            var charCount = text.length;

                            scope.overflow = charCount > scope.limit;
                            scope.moreText = text.slice(0, scope.limit);
                            scope.lessText = text.slice(scope.limit, charCount);
                        }
                    });

                    scope.showMore = function () {
                        scope.readMore = true;
                    };

                    scope.showLess = function () {
                        scope.readMore = false;
                    };
                }
            };
        }]);

    var $keyDirective = function (name, keyName) {
        angular.module('core.directives')
            .directive(name, function ($events) {
                return {
                    link: function (scope, element, attrs) {
                        $events.$keydownListener(scope, element, $events.KEYS[keyName], attrs[name]);
                    }
                };
            });
    };

    $keyDirective('tabKeyEvent', 'TAB');
    $keyDirective('enterKeyEvent', 'ENTER');
})();