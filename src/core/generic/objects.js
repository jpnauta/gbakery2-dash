import { CoreExtLodashModule } from "../ext/lodash";
import { startsWith, omitBy } from "lodash";

/**
 * Removes any private variables from the object
 * @param data {*|Object} data object to filter
 * @return {*|Object}
 */
function omitPrivateVariables(data) {
  return data && omitBy(data, (v, key) => {
    return startsWith(key, '$');
  });
}

export const CoreGenericObjectsModule = angular
    .module('core.generic.objects', [CoreExtLodashModule.name])
    // Put useful object related utilities in here
    .constant('objectUtils', {
      omitPrivateVariables,
    });


