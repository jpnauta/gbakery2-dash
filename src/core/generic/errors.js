export const CoreGenericErrorsModule = angular.module('core.generic.errors', [])
    .service('errorUtils', $log => ({
      /**
       * Converts an error object from the server into a string message
       * which the user can read
       * @param error the Angular error object
       */
      serverErrorToString: function (error) {
        if (error) {
          if (error.status == 400) {
            if (error.data) {
              return error.data.detail;
            }
          } else if (error.status == 500) {
            return '500 Internal Server Error. Whoops, looks like something broke. If the problem persists please contact us!';
          } else if (error.status == 502) {
            return '502 Bad Gateway. Looks like something is temporarily unavailable, please try again soon! If the problem persists please contact us.';
          } else if (error.status == 503) {
            return '503 Service Unavailable. Looks like something is temporarily unavailable, please try again soon! If the problem persists please contact us.';
          } else if (error.status == 504) {
            return '504 Gateway Timeout. Too many people are on this page and it broke the internet! Please try again soon.';
          } else if (error.status == 404) {
            return '404 Not Found. We can\'t find the page you are looking for, please try again!';
          } else if (error.status == -1) {
            return 'Please check your internet connection and try again.';
          } else if (error.status == 403) {
            // Show permission errors
            if (error.data && error.data.detail && error.data.detail != 'FORBIDDEN') {
              return error.data.detail;
            }

            return '403 Forbidden. Nice try, but you don’t have permission to be here!';
          } else if (error.status == 401) {
            return '401 Unauthorized. You might not be logged in, please try again!';
          } else if (error.status == 413) {
            return 'Too much data was sent to the server. Please try a smaller file!';
          }
        }

        // Couldn't determine cause of error!
        $log.error('Unknown error: ', error);
        return 'Something broke, and we are not sure what happened. Please try again - if the problem persists please contact us.';
      }
    }));
