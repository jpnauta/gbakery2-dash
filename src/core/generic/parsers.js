import { CoreGenericDatesModule } from "./dates";
import { CoreExtLodashModule } from "../ext/lodash";

export const CoreGenericParsersModule = angular
    .module('core.generic.parsers', [CoreGenericDatesModule.name, CoreExtLodashModule.name])
    .provider('parsers', function (_) {
      var parsers = [];
      var self = this;

      function register(parser) {
        parsers.push(parser);
      }

      function transformResponse(obj) {
        _.each(parsers, parser => {
          obj = parser(obj);
        });

        return obj;
      }

      self.register = register;
      self.transformResponse = transformResponse;

      self.$get = () => self;
    })
    .config(($httpProvider, parsersProvider) => {
      // Register parser's primary transformer
      $httpProvider.defaults.transformResponse.push(parsersProvider.transformResponse);
    });