import ngResource from 'angular-resource'
import { CoreGenericParsersModule } from "./parsers";
import { CoreExtLodashModule } from "../ext/lodash";

export const CoreGenericModelsModule = angular.module('core.generic.models', [CoreGenericParsersModule.name, CoreExtLodashModule.name, ngResource])
/**
 * @ngdoc Creates a generic model class. Wrapper class for angular-resource objects.
 *
 * Each model class has the the following methods:
 * - MyModel.get({id: 12}).$promise;      // = GET /my-models/12/
 * - MyModel.query({limit: 10}).$promise;      // = GET /my-models/?limit=10
 *
 * Each model instance has the following methods:
 * - myModelInstance.save();     // = POST /my-models/, OR PUT /my-models/{pkField}/ if pkField value is given
 * - myModelInstance.$save();    // = POST /my-models/
 * - myModelInstance.$update();    // = PUT /my-models/{pkField}/
 * - myModelInstance.$delete();    // = DELETE /my-models/{pkField}/
 *
 * @param opts {object} model options
 * - baseUrl: The base URL of the view
 * - paginated: Whether or not the view is paginated
 * - pkField: The primary key used for creating and updating
 * - actions: Custom angular-resource actions
 *
 * For more information, please read
 * [angular resource's documentation](https://docs.angularjs.org/api/ngResource/service/$resource).
 *
 */
    .provider('ModelFactory', function (_) {
      var $initPaginationResults = (pagination, Model) => {
        if (pagination && pagination.results) {
          pagination.results = _.map(pagination.results, obj => new Model(obj));
        }
      };

      var defaults = {
        baseUrl: '/api/', //The base URL of the view
        pkField: 'id', //The primary key used for creating, updating
        actions: {}, //Any custom actions
        channel: null,  // Required for Django channels subscription
        paginated: true,
        domainUrl: '',
        defaultParams: {
          venue: '@venue',
          id: '@id'
        }
      };

      this.setDomainUrl = url => {
        defaults.domainUrl = url;
      };

      this.$get = ($resource, parsers) => opts => {
        opts = _.assign(_.cloneDeep(defaults), opts);

        //Build angular-resource url
        var url = opts.domainUrl + opts.baseUrl + ':preListCtrl/:' + opts.pkField + '/:listCtrl/:docCtrl/';

        //Build angular-resource param defaults
        var paramDefaults = {};
        paramDefaults[opts.pkField] = '@' + opts.pkField;

        //Build angular-resource actions
        var actions = _.assign({
          query: {
            method: 'GET',
            transformResponse: function (data) {
              var objs = angular.fromJson(data);

              //If paginated, initialize each result object as new resource object
              if (opts.paginated) {
                $initPaginationResults(objs, Model);
              }

              return parsers.transformResponse(objs);
            },
            isArray: !opts.paginated
          },
          paginated: {
            method: 'GET',
            transformResponse: function (data) {
              var objs = angular.fromJson(data);
              $initPaginationResults(objs, Model);
              return parsers.transformResponse(objs);
            },
            isArray: false
          },
          update: {  //Used for updating objects
            method: 'PUT'
          },
          save: {
            method: 'POST'
          },
          delete: {
            method: 'DELETE'
          },
          all: {
            method: 'GET',
            listCtrl: 'all',
            transformResponse: function (data) {
              var objs = angular.fromJson(data);
              $initPaginationResults(objs, Model);
              return parsers.transformResponse(objs);
            }
          }
        }, opts.actions);

        // Apply default parameters to each action
        _.each(actions, action => {
          action.params = _.defaults(action.params, opts.defaultParams);
        });

        var Model = $resource(url, paramDefaults, actions);

        Model.prototype.save = function () {
          /**
           * Saves the current object. If the objects primary key
           * is defined, the update function will be called. If not,
           * the create function will be called.
           */
          if (angular.isDefined(this[opts.pkField])) {
            return Model.update(this).$promise;
          } else {
            return this.$save();
          }
        };

        Model.$opts = opts;  // Expose model options

        return Model;
      };
    })
    .service('modelUtils', _ => ({
      venueBasedUrl: function (endUrl, opts) {
        opts = _.assign({
          name: 'venue'
        }, opts);

        return '/api/venue/:' + opts.name + endUrl;
      },

      venueBasedDefaultParams: function (opts) {
        opts = _.assign({
          name: 'venue'
        }, opts);

        return {
          venue: '@' + opts.name,
          id: '@id'
        };
      },

      /**
       * Converts a list of IDs into a map of keys.
       *
       * Useful for easily doing checkboxes on the frontend.
       */
      uniqueListToKeyMap: function (idList) {
        var map = {};
        _.each(idList, val => {
          map[val] = true;
        });
        return map;
      },

      /**
       * Converts a map of booleans back into a list of IDs.
       *
       * Useful for easily doing checkboxes on the frontend.
       */
      keyMapToUniqueList: function (boolMap) {
        var list = [];
        _.each(boolMap, (val, key) => {
          if (val) {
            list.push(key);
          }
        });

        return list;
      }
    }));
