import { CONFIG } from "../../dash/config";

/**
 * Renders the given template
 * @param fn template import function
 * @return {String} rendered HTML
 * @example
 *  const template = renderTemplate(require('./template.pug'));
 */
export function renderTemplate(fn) {
  return fn(CONFIG);
}