import { CoreGenericDeprecationModule } from "./deprecation";
import { CoreExtLodashModule } from "../ext/lodash";

import './pagination.scss';

export const CoreGenericPaginationModule = angular
    .module('core.generic.pagination', [CoreExtLodashModule.name, CoreGenericDeprecationModule.name])
    .service('Pagination', (_, deprecation) => {
      function Pagination(func) {
        deprecation.pending.notify('use pageManager instead');

        this.$init = function (init) {
          _.extend(this, init);
        };

        this.hasMorePages = function () {
          return this.num_pages > 1;
        };

        this.goTo = num => {
          func(num);
        };

        this.reset = function () {
          this.goTo(1);
        };
      }

      return Pagination;
    })
    .directive('paginator', () => ({
      scope: { p: '=paginator' },
      templateUrl: 'core/generic/pagination/paginator.html',

      link: function (scope, element, attrs) {
        //If no results on current page & there's a previous page, go to it
        scope.$watch('p.page_number', () => {
          var p = scope.p;
          if (p && p.previous_page_number && p.results.isEmpty()) {
            p.goTo(p.previous_page_number);
          }
        });
      }
    }))
    /**
     * Loads more pagination whenever triggered. Used with [ngInfiniteScroll](https://github.com/sroze/ngInfiniteScroll)
     * to create infinite scrolling.
     */
    .service('LoadMore', ($rootScope, $q, _) => function (loadPage, context, opts) {
      opts = _.assign({
        start: true
      }, opts);

      var scope = this;

      scope.$loadSet = 1; //Incremented whenever reset occurs, to avoid load threading issues
      scope.$inProgress = false; // The page currently being loaded

      //Retrieves the current page
      scope.$$loadPage = () => {
        var loadSet = scope.$loadSet;
        scope.$inProgress = true; // Indicate that load is occurring

        return $q.when(loadPage(scope.$page, context)).then(page => {
          if (loadSet == scope.$loadSet) { //Don't add results if reset has occurred
            scope.results = scope.results.concat(page && page.results || []);
            scope.count = page && page.count;
            scope.hasMore = Boolean(page && page.next_page_number);
            scope.$inProgress = false; //Reset loading flag
          }
          return page;
        })['catch']((error) => {
          if (loadSet == scope.$loadSet) {
            scope.$inProgress = false; //Reset loading flag
          }
          scope.hasMore = false;
          return $q.reject(error);
        });
      };

      scope.loadMore = () => {
        if (!scope.$inProgress && scope.hasMore) {
          scope.$page += 1;
          return scope.$$loadPage();
        }
      };

      scope.reset = () => {
        scope.count = null;
        scope.$loadSet += 1; // Cancel all pending page loads
        scope.$page = 1;
        scope.results = [];
        scope.hasMore = true;
        return scope.$$loadPage();
      };

      scope.start = () => {
        if (!scope.active) {
          scope.active = true;
          return scope.reset();
        }
      };

      if (opts.start) {
        scope.start();
      }
    })
    /**
     * Manages the loading of a single page. Used for pagination without
     *
     * @param loadPage function to load specified page
     *      Given: page number, context
     * @param context context data to pass to loadPage function (which, in turn, should be passed to server)
     */
    .service('pageManager', (_, $q) => {
      function PageManager(loadPage, context) {
        var scope = this;

        scope.$loadSet = 1; //Incremented whenever reset occurs, to avoid load threading issues
        scope.loading = false; // The page currently being loaded

        scope.$context = _.defaults(_.cloneDeep(context), {
          page_size: 10
        });

        //Retrieves the specified page
        scope.$$load = () => {
          scope.$loadSet += 1; // Cancel all pending page loads
          scope.loading = true; // Indicate that load is occurring

          var loadSet = scope.$loadSet;
          var pageNum = scope.$pageNum;

          context = _.cloneDeep(scope.$context);
          context.page = pageNum;

          var promise = $q.when();
          if (!_.isUndefined(pageNum)) {  // Only load page if page is defined - otherwise, stop action indicated
            promise = $q.when(loadPage(context));
          }

          return promise
              .then(page => {
                if (loadSet == scope.$loadSet) { //Don't add results if reset has occurred
                  scope.page = page;
                }
              })
              .finally(() => {
                // Reset loading
                scope.loading = false;
              });
        };

        /**
         * Go to the specified page number. If undefined/null is given, it will stop the page manager
         * @param num
         * @return {Promise} promise with page loaded, or undefined if page never changed
         */
        scope.$goTo = num => {
          scope.$pageNum = num;
          scope.page = null; // Delete current page

          return scope.$$load();
        };

        /**
         * @return {boolean} whether or not there is a page before the current one, or undefined if unknown
         */
        scope.hasPrevious = () => {
          if (scope.page) {
            return Boolean(scope.page.previous_page_number);
          }
        };

        /**
         * Goes to the previous page, if possible
         * @return {Promise} promise with page loaded, or undefined if not possible
         */
        scope.goToPrevious = () => {
          if (scope.hasPrevious()) {
            return scope.$goTo(scope.page.previous_page_number);
          }
        };

        /**
         * @return {boolean} whether or not there is a page after the current one, or undefined if unknown
         */
        scope.hasNext = () => {
          if (scope.page) {
            return Boolean(scope.page.next_page_number);
          }
        };

        /**
         * Goes to the next page, if possible
         * @return {Promise} promise with page loaded, or undefined if not possible
         */
        scope.goToNext = () => {
          if (scope.hasNext()) {
            return scope.$goTo(scope.page.next_page_number);
          }
        };

        /**
         * Starts the page manager, loading the first page if needed.
         * @return {Promise} promise with page loaded, or undefined if already started
         */
        scope.start = () => scope.$goTo(1);

        /**
         * Stops the page manager, clearing all data
         */
        scope.stop = () => {
          scope.$goTo(undefined);
        };
      }

      return (loadPage, context) => new PageManager(loadPage, context);
    })
    .directive('pageManagerDisplay', () => ({
      scope: {
        'manager': '=pageManagerDisplay'
      },

      templateUrl: 'core/generic/pagination/page-manager-display.html'
    }));
