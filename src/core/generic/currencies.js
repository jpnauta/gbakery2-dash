export const CoreGenericCurrenciesModule = angular
    .module('core.generic.currencies', [])
    //Directive for currency inputs
    .directive('currency', () => ({
      link: function (elem, $scope, attrs) {
        attrs.$set('type', 'number');
        attrs.$set('step', '0.01');
      }
    }));
