import { CoreExtLodashModule } from "../ext/lodash";
import { CoreGenericErrorsModule } from "./errors";

export const CoreGenericMessagesModule = angular.module('core.generic.messages', [CoreGenericErrorsModule.name, CoreExtLodashModule.name])
/**
 * Handles displaying messages to the user
 */
    .service('$notifier', ($mdToast, errorUtils, $q) => {
      var show = (message, icon, toastClass, opts) => {
        $mdToast.show({
          controller: 'ToastCtrl',
          templateUrl: 'toast-template.html',
          toastClass: toastClass,
          locals: {
            message,
            icon,
            opts
          },
          hideDelay: 6000,
          position: 'top right'
        });
      };

      return {
        success: function (message, opts) {
          show(message, 'fa-check', 'success', opts);
        },
        error: function (message, opts) {
          show(message, 'fa-remove', 'error', opts);
        },
        serverErrorHandler: function (error) {
          show(errorUtils.serverErrorToString(error), 'fa-remove', 'error');

          return $q.reject(error);
        }
      };
    })
    .run($templateCache => {
      var toast = `
            <md-toast class="md-toast notifier" ng-class="toastClass">
                <div>
                    <button class="md-primary status" ng-click="close()">
                      <i class="fa" ng-class="icon"></i>
                    </button>
                </div>
                &nbsp;
                <span flex ng-bind="message" class="message"></span>
                <md-button ng-click="close()" ng-href="[[ opts.action.href ]]" ng-bind="opts.action.message"></md-button>
            </md-toast>`;

      //Import default toast HTML template
      $templateCache.put('toast-template.html', toast);
    })
    .controller('ToastCtrl', ($scope, $mdToast, message, icon, toastClass, opts, _) => {
      $scope.message = message;
      $scope.icon = icon;
      $scope.toastClass = toastClass;

      opts = _.assign({
        action: {
          message: 'Close',
          href: null
        }
      }, opts);
      $scope.opts = opts;

      $scope.close = () => {
        $mdToast.hide();
      };
    });
