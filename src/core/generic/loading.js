import { CoreExtLodashModule } from "../ext/lodash";

export const CoreGenericLoadingModule = angular.module('core.generic.loading', [CoreExtLodashModule.name])
/**
 * Sets a certain flag while a promise is occurring. Useful for showing a loading indicator
 * while data is loaded.
 *
 * @param field {String} the name to flag field to change
 * @param opts {object} Options
 *  - scope: the scope which the flag is applied to
 *
 *  @example
 *      // Javascript
 *      var load = Loading('$loading');
 *      load(MyModel.query().$promise);  // Sets $rootScope.$loading to 'true' while loading data
 *
 *      // HTML
 *      <div ng-if="$loading">Show loading screen while data is loaded</div>
 *      <div ng-if="!$loading">Show once data is loaded</div>
 */
    .service('Loading', ($rootScope, $q, $loaderUtils, loadingSettings, _) => {
      function Loading(field, opts) {
        var $scope = {};

        opts = _.assign({
          notifyOnSuccess: false,
          notifyOnError: false,
          scope: $rootScope
        }, opts);
        $scope.$$currentLoadID = 1;

        var $set = val => {
          opts.scope[field] = val;
        };

        return promise => {
          $scope.$$currentLoadID += 1; //Indicate that new load has begun

          promise = $scope.$current = $q.when(promise);
          var loadID = $scope.$$currentLoadID;

          if (opts.notifyOnSuccess) {
            $loaderUtils.successDisplay(promise, loadingSettings.successHandler);
          }

          if (opts.notifyOnError) {
            $loaderUtils.errorCatch(promise, loadingSettings.errorCatchHandler);
          }

          $set(true);
          return promise['finally'](data => {
            if (loadID == $scope.$$currentLoadID) { //If another load was invoked since, don't change state
              $set(null);
              return data;
            }
          });
        };
      }

      return Loading;
    })
    .service('loadingSettings', () => {
      var settings = {
        successHandler: function (message) {
        },  // Function called when load is successful
        errorCatchHandler: function (error) {
        }  // Function called when load error occurs
      };

      return settings;
    })
    .service('$loaderUtils', $q => ({
      errorCatch: function (promise, handler) {
        return $q.when(promise)['catch'](error => {
          handler(error);  // Run configured error handler
          return $q.reject(error);
        });
      },

      successDisplay: function (promise, handler) {
        return $q.when(promise)
            .then(handler);
      }
    }))
    .service('Delayer', ($timeout, _) => {
      function Delayer(func, opts) {

        opts = _.assign({
          time: 1000,
          timeFunc: function () {
            return opts.time;
          }
        }, opts);

        let self = this;

        self.$promise = null;
        self.$loaders = [];

        /**
         * Invokes the given function in the specified time. If
         * self function is called again before the timer ends,
         * it will reset the timer
         */
        self.invoke = function () {
          self.cancel();
          var promise = $timeout(func, opts.timeFunc());

          _.each(self.$loaders, load => {
            load(promise);
          });

          self.$promise = promise;
          return promise;
        };

        self.$addLoadFunction = function (load) {
          self.$loaders.push(load);
        };

        self.cancel = function () {
          //Cancel the pending function call, if any
          $timeout.cancel(self.$promise);
          self.$promise = null;
        };
      }

      return Delayer;
    })
    .service('LoaderFactory', function ($utils, Loading, _) {
      return {
        $loader: function (opts) {
          opts = _.assign({
            property: '$loading'
          }, opts);

          return new Loading(opts.property, opts);
        },
        //Loader that will display a logical notification, on success or fail event
        standard: function (opts) {
          var load = this.$loader(opts);

          return function (promise, opts) {
            load(promise);
            return $utils.$successDisplay($utils.$errorCatch(promise), opts);
          };
        },
        //Loader that will catch errors & display them, but won't do anything on success
        errorOnly: function (opts) {
          var load = this.$loader(opts);

          return function (promise, opts) {
            load(promise);
            return $utils.$errorCatch(promise, opts);
          };
        },
        silent: function (opts) {
          var load = this.$loader(opts);

          return function (promise, opts) {
            return load(promise);
          };
        }
      };
    })
    .directive('btnLoading', function () {
      return {
        scope: {
          btnLoading: '&',
          btnLoadingText: '@',
          btnLoadingIcon: '@'
        },
        template: '<span>' +
            '<span ng-if="!btnLoading()">' +
            '<i ng-class="btnLoadingIcon"></i>' +
            '<span ng-bind="btnLoadingText"></span>' +
            '</span>' +
            '<span ng-if="btnLoading()">' +
            '<i class="icon-spin icon-spinner"></i> ' +
            '<span>Loading</span>' +
            '</span>' +
            '</span>'

      };
    });
