import { CoreExtLodashModule } from "../ext/lodash";
import { CoreExtMomentModule } from "../ext/moment";
import { CoreGenericDeprecationModule } from "./deprecation";

export const CoreGenericFiltersModule = angular
    .module('core.generic.filters', [CoreExtLodashModule.name, CoreExtMomentModule.name, CoreGenericDeprecationModule.name])
    .filter('money', $filter => // TO BE DEPRECATED - use currency filter instead
        $filter('currency'))
    .filter('percentage', () => input => Number(input).toFixed(1) + '%')
    .filter('abs', () => input => Math.abs(input))
    .filter('plusMinus', () => input => {
      if (input > 0) {
        return '+' + input;
      } else {
        return String(input);
      }
    })
    .filter('fromNow', moment => (input, simple) => {
      if (input) {
        return moment(input).fromNow(simple);
      }
    })
    //Removes any HTML elements from text
    .filter('stripHTML', () => text => String(text).replace(/<[^>]+>/gm, ''))
    .filter('rangeStartEnd', () => (input, start, end) => {
      for (var i = parseInt(start); i <= parseInt(end); i++) {
        input.push(i);
      }
      return input;
    })
    .filter('months', () => input => {
      input = [
        { 'name': 'January', 'val': 1 },
        { 'name': 'February', 'val': 2 },
        { 'name': 'March', 'val': 3 },
        { 'name': 'April', 'val': 4 },
        { 'name': 'May', 'val': 5 },
        { 'name': 'June', 'val': 6 },
        { 'name': 'July', 'val': 7 },
        { 'name': 'August', 'val': 8 },
        { 'name': 'September', 'val': 9 },
        { 'name': 'October', 'val': 10 },
        { 'name': 'November', 'val': 11 },
        { 'name': 'December', 'val': 12 }
      ];
      return input;
    })
    //Given # bytes, displays # megabytes
    .filter('megabytes', _ => val => {
      if (_.isFinite(val)) {
        return (val / 1024 / 1024).toFixed(2) + ' MB';
      }
    })
    //Filters by whether or not the specified field is defined
    .filter('includeIf', _ => (arr, field, val) => {
      if (!angular.isDefined(val)) {
        val = true;
      }

      return _.filter(arr, obj => Boolean(obj[field]) == val);
    })
    //Checking if a js object is empty
    .filter('isEmpty', $utils => obj => $utils.dictIsEmpty(obj))
    .filter('range', () => (input, total) => {
      total = parseInt(total);

      for (var i = 0; i < total; i++) {
        input.push(i);
      }

      return input;
    })
    /**
     * Capitalizes the first letter in the string
     */
    .filter('startCase', _ => str => _.startCase(str))

    .filter('reverse', _ => items => (items || []).slice().reverse())
    .filter('parseTime', function ($utils) {
      return function (text) {
        return $utils.parseTime(text);
      };
    })
    .filter('pluck', function (_) {
      return function (arr, field) {
        return _.pluck(arr, field);
      };
    })
    .filter('phoneNumber', function ($utils) {
      return function (text) {
        return $utils.formatPhoneNumber(text);
      };
    })
    .filter('contains', function (_) {
      return function (arr, val) {
        return _.find(arr, function (obj) {
          return _.isEqual(obj, val);
        });
      };
    });

