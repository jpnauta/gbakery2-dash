import { CoreExtLodashModule } from "../ext/lodash";
import { CoreExtMomentModule } from "../ext/moment";

var ISO_8601_FULL = /^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d+)?(([+-]\d\d:\d\d)|Z)?$/i;

function convertDateStringsToDates(input) {
  if (typeof input === 'object') {
    for (var key in input) {
      if (!input.hasOwnProperty(key)) {
        continue;
      }

      var value = input[key];
      var match;
      // Check for string properties which look like dates.
      if (typeof value === 'string' && (match = value.match(ISO_8601_FULL))) {
        // Assume that Date.parse can parse ISO 8601 strings, or has been shimmed in older browsers to do so.
        var milliseconds = Date.parse(match[0]);
        if (!isNaN(milliseconds)) {
          input[key] = new Date(milliseconds);
        }
      } else if (typeof value === 'object') {
        // Recurse into object
        convertDateStringsToDates(value);
      }
    }
  }

  return input;
}

export const CoreGenericDatesModule = angular
    .module('core.generic.dates', [CoreExtMomentModule.name, CoreExtLodashModule.name])
    .constant('$dateConstants', {
      ISO_8601_FULL,
      convertDateStringsToDates
    })
    .factory('$dateUtils', (moment, _) => {
      function getRanges() {
        var now = moment()._d;
        var weekend_start = moment().day(5).startOf('day').add(17, 'hours')._d; // From Friday 5pm
        var weekend_end = moment().day(7).endOf('day').add(2, 'hours')._d;    //  to Monday 2am

        if (now > weekend_start) {
          weekend_start = now;
        }

        return {
          'All Dates': [now, null],
          'Today': [moment().startOf('day')._d, moment().endOf('day')._d],
          'Tomorrow': [moment().add(1, 'day').startOf('day')._d, moment().add(1, 'day').endOf('day')._d],
          'This Week': [now, weekend_end],
          'This Weekend': [weekend_start, weekend_end],
          'This Month': [now, moment().endOf('month')._d],
          'Next Month': [moment().add(1, 'month').startOf('month')._d, moment().add(1, 'month').endOf('month')._d]
        };
      }

      function getRange(key) {
        var ranges = getRanges();
        if (key in ranges) {
          return ranges[key];
        } else {
          return undefined;
        }
      }

      function makeMonths() {
        return _.map(moment.months(), (month, key) => ({
          val: key + 1,
          name: month
        }));
      }

      return {
        getRange,
        getRanges,
        makeMonths
      };
    })

    .filter('timezone', moment => (dt, tz, format) => moment.tz(dt, tz).format(format))
    .filter('timezoneAbbr', moment => (dt, tz) => moment.tz(dt, tz).zoneAbbr())
    /**
     * Parses a `Date` from a model input into a formatted date string
     *
     * @example
     * <md-datepicker ng-model="myDate" format-date="" />  // `myDate` will be in the form `YYYY-MM-DD`
     */
    .directive('formatDate', function (moment) {
      function link(scope, element, attrs, ngModel) {
        let parser = function (val) {
          val = moment(val).format('YYYY-MM-DD');
          return val;
        };
        let formatter = function (val) {
          if (!val) {
            return val;
          }
          val = moment(val).toDate();
          return val;
        };
        ngModel.$parsers.push(parser);
        ngModel.$formatters.push(formatter);
      }

      return {
        require: 'ngModel',
        link: link,
        restrict: 'EA',
        priority: 1
      };
    });
