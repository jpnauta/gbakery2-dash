export const CoreConstantsModule = angular
    .module('core.generic.constants', [])
    .factory('$constants', function (_) {
      var optionsByField = function (opts, field, keyfield) {
        var newOpts = {};
        keyfield = keyfield || 'key';

        _.each(opts, function (module, key) {
          module = _.clone(module);
          module[keyfield] = key;
          newOpts[module[field]] = module;
        });

        return newOpts;
      };

      var optionsAsList = function (opts) {
        return _.map(opts, function (obj) {
          return obj;
        });
      };

      var QUANTITY_TYPE_OPTIONS = {
        QUANTITY_INDIVIDUAL: {
          id: 1,
          name: 'Individual',
          shortName: 'ea.',
          shortName2: '',
          quantity: 1
        },
        QUANTITY_HALF_DOZEN: {
          id: 2,
          name: 'Half Dozen',
          shortName: '½ doz.',
          shortName2: 'half doz.',
          quantity: 6
        },
        QUANTITY_DOZEN: {
          id: 3,
          name: 'Dozen',
          shortName: 'doz.',
          shortName2: 'doz.',
          quantity: 12
        }
      };

      var SIZE_OPTIONS = {
        SIZE_SMALL_ROUND: {
          id: 101,
          name: '7 Inch Round'
        },
        SIZE_LARGE_ROUND: {
          id: 102,
          name: '8 Inch Round'
        },
        SIZE_10_INCH_ROUND: {
          id: 103,
          name: '10 Inch Round'
        },
        SIZE_BABY_CAKE: {
          id: 104,
          name: 'Baby Cake'
        },
        SIZE_7X7: {
          id: 201,
          name: '7x7'
        },
        SIZE_7X10: {
          id: 202,
          name: '7x10'
        },
        SIZE_HALF_SLAB: {
          id: 203,
          name: 'Half Slab'
        },
        SIZE_3_4_SLAB: {
          id: 204,
          name: 'Three Quarter Slab'
        },
        SIZE_FULL_SLAB: {
          id: 205,
          name: 'Full Slab'
        },
        SIZE_12_CUPCAKE_CAKE: {
          id: 301,
          name: '12 Cupcake cake'
        },
        SIZE_24_CUPCAKE_CAKE: {
          id: 302,
          name: '24 Cupcake cake'
        },
        SIZE_INDIVIDUAL_CUPCAKES: {
          id: 303,
          name: 'Individual cupcakes'
        }
      };


      var TYPE_OPTIONS = {
        TYPE_WHITE: {
          id: 201,
          name: 'White'
        },
        TYPE_CHOCOLATE: {
          id: 202,
          name: 'Chocolate'
        },
        TYPE_WHITE_AND_CHOCOLATE: {
          id: 203,
          name: 'White and chocolate cake'
        },
        TYPE_RED_VELVET: {
          id: 204,
          name: 'Red velvet'
        },
        TYPE_CARROT: {
          id: 205,
          name: 'Carrot'
        }
      };


      var FILLING_OPTIONS = {
        F_CUSTARD: {
          id: 301,
          name: 'Vanilla custard'
        },
        F_CHOCOLATE_CUSTARD: {
          id: 302,
          name: 'Chocolate custard'
        },
        F_WHITE_BUTTERCREAM: {
          id: 401,
          name: 'White buttercream'
        },
        F_MOCHA_BUTTERCREAM: {
          id: 402,
          name: 'Mocha buttercream'
        },
        F_CHOCOLATE_BUTTERCREAM: {
          id: 403,
          name: 'Chocolate buttercream'
        },
        F_MINT_BUTTERCREAM: {
          id: 404,
          name: 'Mint buttercream'
        },
        F_LEMON_BUTTERCREAM: {
          id: 404,
          name: 'Lemon buttercream'
        },
        F_WHIP_CREAM: {
          id: 501,
          name: 'Whipcream'
        },
        F_FRUIT_WHIP_CREAM: {
          id: 502,
          name: 'Fresh fruit whipcream'
        },
        F_RASPBERRY_WHIP_CREAM: {
          id: 503,
          name: 'Raspberry whipcream'
        },
        F_STRAWBERRY_WHIP_CREAM: {
          id: 503,
          name: 'Strawberry whipcream'
        },
        F_GRAND_MARNIER: {
          id: 601,
          name: 'Grand marnier'
        },
        F_BLACK_FOREST: {
          id: 602,
          name: 'Black Forest'
        },
        F_HAZELNUT: {
          id: 603,
          name: 'Hazelnut custard'
        },
        F_LEMON_CUSTARD: {
          id: 604,
          name: 'Lemon filling'
        },
        F_CHOCOLATE_GANACHE: {
          id: 605,
          name: 'Chocolate Ganache'
        },
        F_CREAM_CHEESE: {
          id: 701,
          name: 'Cream Cheese'
        }
      };

      var ICING_OPTIONS = {
        ICING_WHITE: {
          id: 401,
          name: 'White'
        },
        ICING_MOCHA: {
          id: 402,
          name: 'Mocha'
        },
        ICING_CHOCOLATE: {
          id: 403,
          name: 'Chocolate'
        },
        ICING_WHIP_CREAM: {
          id: 404,
          name: 'Whipcream'
        },
        ICING_MINT: {
          id: 501,
          name: 'Mint'
        },
        ICING_GRAND_MARNIER: {
          id: 502,
          name: 'Grand Marnier'
        },
        ICING_LEMON: {
          id: 503,
          name: 'Lemon'
        }
      };

      var PRINT_METHOD_OPTIONS = {
        PRINT_THERMAL: {
          id: 'label-paper',
          name: 'Label Paper',
          pageStyle: `
          @page {
            size: 4in 6in;
            margin: 0;
          }
          `
        },
        PRINT_REGULAR: {
          id: 'regular-paper',
          name: 'Regular Paper',
          pageStyle: ''
        }
      };

      return {
        QUANTITY_TYPE_OPTIONS: QUANTITY_TYPE_OPTIONS,
        QUANTITY_TYPES_BY_ID: optionsByField(QUANTITY_TYPE_OPTIONS, 'id'),
        QUANTITY_TYPE_LIST: optionsAsList(QUANTITY_TYPE_OPTIONS),
        SIZE_OPTIONS: SIZE_OPTIONS,
        SIZES_BY_ID: optionsByField(SIZE_OPTIONS, 'id'),
        TYPE_OPTIONS: TYPE_OPTIONS,
        TYPES_BY_ID: optionsByField(TYPE_OPTIONS, 'id'),
        FILLING_OPTIONS: FILLING_OPTIONS,
        FILLINGS_BY_ID: optionsByField(FILLING_OPTIONS, 'id'),
        ICING_OPTIONS: ICING_OPTIONS,
        ICINGS_BY_ID: optionsByField(ICING_OPTIONS, 'id'),
        PRINT_METHOD_OPTIONS: PRINT_METHOD_OPTIONS,
        PRINT_METHOD_LIST: optionsAsList(PRINT_METHOD_OPTIONS),
      };
    });