import angular from 'angular';

export const CoreAnimationModule = angular.module('core.animation', [])
//Fade in when added
    .animation('.animate-fade-in', function () {
      return {
        enter: function (element, done) {
          element.css({
            opacity: 0
          });

          element.animate({
            opacity: 1
          }, done);
        }
      };
    })
    //Fade in when moved
    .animation('.animate-fade-move', function () {
      return {
        move: function (element, done) {
          element.css({
            opacity: 0
          });

          element.animate({
            opacity: 1
          }, done);
        }
      };
    })
    //Fade out when removed
    .animation('.animate-fade-out', function () {
      return {
        leave: function (element, done) {
          element.css({
            opacity: 1
          });

          element.animate({
            opacity: 0
          }, done);
        }
      };
    })
    //Shrink when added
    .animation('.animate-shrink-in', function () {
      return {
        enter: function (element, done) {
          var height = element.height();
          var width = element.width();
          element.css({
            opacity: 0,
            'max-height': 0,
            'max-width': 0
          });

          element.animate({
            opacity: 1,
            overflow: 'hidden',
            'max-height': height,
            'max-width': width
          }, function () {
            element.css({
              'max-height': '',
              'max-width': ''
            });

            done();
          });
        }
      };
    })
    //Shrink when added
    .animation('.animate-shrink-out', function () {
      return {
        leave: function (element, done) {
          element.css({
            overflow: 'hidden',
            'max-height': element.height(),
            'max-width': element.width()
          });

          element.animate({
            opacity: 0,
            'max-height': 0,
            'max-width': 0
          }, done);
        }
      };
    });