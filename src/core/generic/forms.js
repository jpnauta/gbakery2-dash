import ngMessages from 'angular-messages';
import { CoreExtLodashModule } from "../ext/lodash";

export const CoreGenericFormsModule = angular
    .module('core.generic.forms', [CoreExtLodashModule.name, ngMessages])
    /**
     * Utility functions for Angular forms
     */
    .provider('appForms', function (_) {
      let errorMessages = {
        // DEFAULT ERROR MESSAGES FOR ENTIRE SYSTEM - if you want to add/override to this list, you
        // probably want to use `defaultErrorMessages`
        required: 'Field is required',
        minlength: 'Not enough characters',
        maxlength: 'Too many characters',
        integer: 'Invalid number'
      };

      function mergeMessages(m1, m2) {
        return _.merge(_.clone(m1), m2);
      }

      /**
       * Set/overrides default error messages for entire app
       * @param messages {Object} key-value map of error ID and their corresponding message
       */
      function defaultErrorMessages(messages) {
        errorMessages = mergeMessages(errorMessages, messages);
      }

      this.defaultErrorMessages = defaultErrorMessages;

      this.$get = function () {
        let appForms = {
          /**
           * Resets the form to its original pristine state
           * @param form angular form
           */
          resetForm: form => {
            if (!_.isNil(form)) {
              form.$setPristine();
              form.$setUntouched();
            }
          },
          /**
           * Triggers the $setTouched function for each field in the form. Useful
           * for highlighting fields red when form is attempted to be submitted, but is
           * invalid.
           * @param form angular form
           */
          setErrorFieldsTouched: form => {
            angular.forEach(form, field => {
              angular.forEach(field, errorField => {
                if (errorField && errorField.$setTouched) {
                  errorField.$setTouched();
                }
              });
            });
          },
          /**
           * Gets a error message map
           * @param messages {Object} custom error messages
           * @return {Object} key-value map of error ID and their corresponding message
           */
          getErrorMessages: function (messages) {
            return mergeMessages(errorMessages, messages);
          },
          /**
           * Gets the first error value
           * @param field {Object} The form field tied to the error
           * @param messages {Object} custom error messages
           * @return {string} error message corresponding to the first error message raised
           */
          getFirstErrorMessage(field, messages) {
            let errorList = appForms.getErrorMessages(messages);
            let error = field.$error;
            if (!_.isEmpty(error)) {
              return errorList[Object.keys(error)[0]];
            }
          }
        };

        return appForms;
      };
    })
    /**
     * Custom form submit. Acts similar to `ng-submit`, with several minor differences:
     *
     * - Does not submit when form is `$invalid`
     * - Sets each input field to `$touched` if invalid
     * - `$submitting` flag is set while promise is being resolved
     *
     * @example
     * <form name="myForm" app-form-submit="mySubmit()">
     *   <button ng-disabled="myForm.$submitting"></button>
     * </form>
     */
    .directive('appFormSubmit', ($q, appForms) => {
      return {
        restrict: 'A',
        link: function (scope, element, attributes) {
          if (attributes['novalidate'] === undefined) {
            attributes.$set('novalidate', 'novalidate');
          }

          element.bind('submit', function (e) {
            e.preventDefault();

            // Get the form object
            let form = scope.$eval(attributes.name);

            if (form.$valid) {
              // If the form is valid call the method specified
              let promise = scope.$eval(attributes.appFormSubmit);

              // Set/reset `$submitting` flag
              form.$submitting = true;
              $q.when(promise)
                  .finally(function () {
                    form.$submitting = undefined;
                  });
            } else {
              // 'Touch' all fields to indicate to user all invalid fields
              appForms.setErrorFieldsTouched(form);

              // Click on the first invalid field
              element.find('.ng-invalid').first().focus();
            }
          });
        }
      };
    })
    /**
     * Uses `ngMessages` to display form error messages. Uses configured default error messages unless
     * custom message is explictly specified
     *
     * @example
     * <form name="myForm">
     *  <input type="text" name="myField" ng-maxlength="20">
     *  <app-error-messages field="myForm.myField" messages="{errorKey: 'Custom Error Message'}"></app-error-messages>
     * </form>
     */
    .directive('appErrorMessages', (appForms) => {
      return {
        restrict: 'E',
        priority: 999,
        scope: {
          field: '=field',
          customMessages: '=messages'
        },
        template: `<div ng-messages="field.$error">
                          <div ng-repeat="(key, value) in messages" ng-message="[[ key ]]">
                            <span ng-bind="value"></span>
                          </div>
                       </div>`,
        link: (scope, element, attributes) => {
          scope.$watch('customMessages', function (messages) {
            scope.messages = appForms.getErrorMessages(messages);
          });
        }
      };
    });
