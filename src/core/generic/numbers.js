export const CoreGenericNumbersModule = angular.module('core.generic.numbers', [])
//Ensures an input value is an integer
    .directive('integer', () => ({
      require: 'ngModel',

      link: function (scope, elm, attrs, ctrl) {
        var INTEGER_REGEXP = /^\-?\d*$/;

        attrs.$set('step', 1); //Set step attribute to 1 (convenience)

        ctrl.$parsers.unshift(viewValue => {
          if (INTEGER_REGEXP.test(viewValue)) {
            ctrl.$setValidity('integer', true);
            return viewValue;
          } else {
            ctrl.$setValidity('integer', false);
            return undefined;
          }
        });
      }
    }));
