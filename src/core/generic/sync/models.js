import ReconnectingWebSocket  from 'reconnecting-websocket';

import { CoreGenericModelsModule } from "../models";
import { CoreExtLodashModule } from "../../ext/lodash";
import { CoreExtConfigModule } from "../../ext/config";

/* @ngInject */
const ModelBridge = ($log, _, CONFIG, $q) => {
  /**
   * Wrapper for a websocket connection
   */
  class ModelBridge {
    /**
     * Creates a bridge for a specific model class
     * @param modelClass class created from `ModelFactory`
     */
    constructor(modelClass) {
      this.modelClass = modelClass;

      this.socket = null;
      this.connected = false;
      this.onOpenListeners = [];
      this.onMessageListeners = [];

      // Ensure channel set on model class
      this.channel = this.modelClass.$opts.channel;
      if (!this.channel) {
        throw Error('channel field not specified');
      }
    }

    get channelUrl() {
      return `${CONFIG.app.url.replace('http', 'ws')}/${this.channel}`;
    }

    /**
     * Start a connection with the websocket server. See
     * `channels.WebSocketBridge.connect` for more info.
     */
    connect() {
      // Init websocket
      this.socket = new ReconnectingWebSocket(this.channelUrl);
      this.socket.onmessage = (r) => this.onmessage(r);
      this.socket.onclose = (err) => this.onclose(err);
      this.socket.onopen = () => this.onopen();

      this.connected = false;
    }

    onmessage(response) {
      for (let listener of this.onMessageListeners) {
        const message = JSON.parse(response.data);
        listener(message);
      }
    }

    onopen() {
      this.connected = true;
      for (let listener of this.onOpenListeners) {
        listener();
      }
    }

    onclose(err) {
      this.connected = false;
    }

    /**
     * Adds a onOpen listener function to the bridge
     * @param cb Callback triggered on open
     */
    onOpen(cb) {
      this.onOpenListeners.push(cb);
    }

    /**
     * Adds a onMessage listener function to the bridge
     * @param cb Callback that receives message
     */
    onMessage(cb) {
      this.onMessageListeners.push(cb);
    }

    /**
     * Sends a message to the model's channel.
     * `connect` for more info.
     */
    send(message) {
      this.socket.send(JSON.stringify(message));
    }

    /**
     * Changes the given list of results according to the change message given
     * @param message subscription message sent from server
     * @param arr the old list of results
     * @returns {Array} the new list of results
     */
    updateResultList(message, arr) {
      arr = arr || [];

      if (message.action == 'create') {
        //Just in case, attempt to find duplicate objects and remove them
        arr = _.filter(arr, obj => obj.id != message.data.id);

        //Add new object to list
        arr = arr.concat(new this.modelClass(message.data));
      } else if (message.action == 'update') {
        //Find old reference to object and replace it with new one
        arr = _.map(arr, obj => obj.id == message.data.id ? _.assign(obj, message.data) : obj);
      } else if (message.action == 'delete') {
        //Remove any references to deleted object
        arr = _.filter(arr, obj => obj.id != message.data.id);
      }

      return arr;
    }

    /**
     * Authenticates the user
     * @param token JWT authentication token
     */
    subscribe(token) {
      this.send({
        action: 'subscribe',
        data: {
          token: token
        }
      });
    }
  }

  /**
   * Opens a websocket subscription and triggers the given callback whenever
   * a subscription message is received
   * @param modelClass class created from `ModelFactory`
   * @param token authentication token
   * @param cb callback function for subscription messages
   * @param reloadData if true, data will be retrieved from server before callback
   * @return {ModelBridge} websocket bridge
   */
  ModelBridge.connectAndSubscribe = (modelClass, token, cb, { reloadData = false } = {}) => {
    let bridge = new ModelBridge(modelClass);

    bridge.connect();
    bridge.onOpen(() => {
      bridge.subscribe(token);
    });
    bridge.onMessage((message) => {  // Listen for unexpected calls (all messages should be multiplexed to `cb`)
      let promise = $q.when();

      // If flag is specified, and object is created or updated
      if (reloadData && (message.action == 'create' || message.action == 'update')) {
        // Reload data from server before callback is called
        const query = {};
        const pkField = modelClass.$opts.pkField;
        query[pkField] = message.data[pkField];
        promise = modelClass.get(query).$promise
            .then((data) => {
              message.data = data;
            });
      }

      promise.then(() => {
        cb(bridge, message);
      });
    });

    return bridge;
  };

  return ModelBridge;
};

export const CoreGenericSyncModelsModule = angular
    .module('core.generic.sync.models', [CoreGenericModelsModule.name, CoreExtLodashModule.name, CoreExtConfigModule.name])
    .service('ModelBridge', ModelBridge, { reloadData: true });