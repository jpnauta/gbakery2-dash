export const CoreGenericStringsModule = angular.module('core.generic.strings', [])
// Put useful string related utilities in here
    .service('stringUtils', () => {
        var _startCaseWithSpecChars = input_string => // http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
            input_string.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
        return {
            startCaseWithSpecialCharacters: _startCaseWithSpecChars
        };
    });
