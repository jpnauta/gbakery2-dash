import { assign } from 'lodash';

/**
 * Simple object to handle code in some stage of being deprecated
 */
function DeprecationHandler(opts) {
  var scope = this;

  scope.opts = assign({
    action: null,
    description: null
  }, opts);

  /**
   * Specifies what deprecation action to take when triggered. See DeprecationHandler.ACTIONS for
   * more info
   *
   * @param action
   */
  scope.setAction = action => {
    scope.opts.action = action;
  };

  /**
   * Notify user of deprecation
   * @param message message to display to user
   */
  scope.notify = message => {
    message = scope.opts.description + ': ' + (message || '(No message)');

    scope.opts.action(message);
  };
}

/**
 * Available actions to use for a DeprecationHandler object
 */
DeprecationHandler.ACTIONS = {
  IGNORE: function (message) { // Do nothing

  },
  INFO: function (message) {  // Log an info message
    /*eslint-disable no-console */
    console.info(new Error(message).stack);
    /*eslint-enable no-console */
  },
  WARNING: function (message) {  // Log a warning message
    /*eslint-disable no-console */
    console.warn(new Error(message).stack);
    /*eslint-enable no-console */
  },
  ERROR: function (message) {  // Cause an error
    throw new Error(message);
  }
};

var deprecation = {
  pending: new DeprecationHandler({
    action: DeprecationHandler.ACTIONS.WARNING,
    description: 'PENDING DEPRECATION'
  }),
  complete: new DeprecationHandler({
    action: DeprecationHandler.ACTIONS.ERROR,
    description: 'DEPRECATED'
  })
};

// Expose deprecation system to Angular
export const CoreGenericDeprecationModule = angular.module('core.generic.deprecation', [])
    .constant('DeprecationHandler', DeprecationHandler)
    .constant('deprecation', deprecation);
