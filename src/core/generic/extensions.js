import { CoreExtLodashModule } from "../ext/lodash";

/**
 * Adds various functions to built-in Javascript classes
 */
export const CoreGenericExtensionsModule = angular.module('core.generic.extensions', [CoreExtLodashModule.name])
/** NUMBERS **/
    .run(_ => {
      Number.zeroPad = (number, width) => {
        width -= number.toString().length;
        if (width > 0) {
          return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
        }
        return number + ''; // always return a string
      };

      /**
       * Rounds to the nearest, 5, 10, 100, etc.
       *
       * Math.roundToNearest(6, 10) => 10
       * Math.roundToNearest(14, 10) => 10
       * Math.roundToNearest(503, 100) => 500
       */
      Math.roundToNearest = (num, val) => Math.round(num / val) * val;

      /**
       * Adds locale comma separators to the given number
       *
       * Number.withSeparators(999) => '999'
       * Number.withSeparators(1000) => '1,000'
       * Number.withSeparators(9999999.99) => '9,999,999.99'
       *
       * @param num The number to specify
       */
      Number.withSeparators = (x, opts) => {
        opts = _.assign({
          separator: ','
        }, opts);

        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, opts.separator);
      };

      /**
       * Converts the given number into a string representing money
       *
       * Number.toMoneyString(4.1) => '$4.10'
       * Number.toMoneyString(2099) => '$2099.00'
       *
       * @param num The number to specify
       */
      Number.toMoneyString = num => {
        var str = '$' + Number.withSeparators(Math.abs(num).toFixed(2));
        if (num < 0) {
          str = '-' + str;
        }
        return str;
      };
    })
    /** ARRAYS **/
    .run(_ => {
      var p = Array.prototype;

      _.mixin({
        sum: function (obj, iterator, context) {
          if (!iterator && _.isEmpty(obj)) {
            return 0;
          }
          var result = 0;
          _.forEach(obj, (value, index, list) => {
            var v = _.isFunction(iterator) ?
                iterator.call(context, value, index, list) :
                _.isString(iterator) ?
                    value[iterator] :
                    value;
            result = result + v;
          });
          return result;
        },
        /**
         * Generates all mathematical combinations of the array of elements
         * @param obj the array to iterate
         * @param x the number of items to choose
         * @returns {*} An array containing all combinations
         *
         * @example [1, 2, 3] => [[1, 2], [1, 3], [2, 3]]
         */
        chooseX: function (obj, x) {
          var i, j, combs, head, tailCombs;

          if (x > obj.length || x <= 0) {
            return [];
          }

          if (x == obj.length) {
            return [obj];
          }

          if (x == 1) {
            combs = [];
            for (i = 0; i < obj.length; i++) {
              combs.push([obj[i]]);
            }
            return combs;
          }

          combs = [];
          for (i = 0; i < obj.length - x + 1; i++) {
            head = obj.slice(i, i + 1);
            tailCombs = obj.slice(i + 1).chooseX(x - 1);
            for (j = 0; j < tailCombs.length; j++) {
              combs.push(head.concat(tailCombs[j]));
            }
          }
          return combs;
        }
      });

      p.chooseX = function (x) {
        return _.chooseX(this, x);
      };

      p.isEmpty = function () {
        return this.length == 0;
      };

      p.sum = function (field) {
        return _.sum(this, field);
      };

      p.remove = function (item) {
        var index = this.indexOf(item);
        if (index > -1) {
          this.splice(index, 1);
        }
      };

      p.endSlice = function (amount) {
        return this.slice(Math.max(this.length - amount, 0), this.length);
      };

      p.allContain = function (prop) {
        return _.filter(this, prop).length == this.length;
      };
    });
