// Un-categorized directives
import { CoreExtLodashModule } from "../ext/lodash";
import { CoreExtJquery } from "../ext/jquery";

export const CoreGenericDirectivesModule = angular
    .module('core.generic.directives', [CoreExtJquery.name, CoreExtLodashModule.name])
    .directive('ngTap', () => (scope, element, attr) => {
      var tapping;
      tapping = false;
      element.bind('touchstart', e => {
        element.addClass('active');
        tapping = true;
      });
      element.bind('touchmove', e => {
        element.removeClass('active');
        tapping = false;
      });
      element.bind('touchend', e => {
        element.removeClass('active');
        if (tapping) {
          scope.$apply(attr['ngTap'], element);
        }
      });
    })
    //Directive that acts the same as ngShow, but for 'visibility: hidden' instead
    .directive('ngVisible', $animate => (scope, element, attr) => {
      scope.$watch(attr.ngVisible, value => {
        $animate[value ? 'removeClass' : 'addClass'](element, 'ng-invisible');
      });
    })
    //Focus on element when model is set to true
    .directive('focusMe', ($timeout, $parse) => ({
      link: function (scope, element, attrs) {
        var model = $parse(attrs.focusMe);

        //Focus on element when value is set to true
        scope.$watch(model, value => {
          if (value === true) {
            $timeout(() => {
              element[0].focus();
            });
          }
        });

        //Revert model value on blur
        element.bind('blur', () => {
          scope.$apply(model.assign(scope, false));
        });
      }
    }))
    //Scroll to element when model is set to true
    .directive('scrollTo', ($timeout, $parse, $) => ({
      link: function (scope, element, attrs) {
        var model = $parse(attrs.scrollTo);

        //Focus on element when value is set to true
        scope.$watch(model, value => {
          if (value === true) {
            $timeout(() => {
              $('html, body').animate({
                scrollTop: element.scrollTop()
              }, 300);
              scope.$apply(model.assign(scope, false));
            });
          }
        });
      }
    }))
    .directive('focusDefault', $document => ({
      link: function (scope, element, attrs) {
        $document.bind('keydown', event => {
          var isSpecialKey = event.ctrlKey || event.metaKey || event.altKey || event.which == 13;

          if (!isSpecialKey) { //Ignore special keys
            var nodeName = event.target.nodeName;

            //If already focused on recieving element, ignore
            if ('INPUT' == nodeName || 'TEXTAREA' == nodeName) {
              return;
            }

            //Otherwise, focus on given element
            element.val('').trigger('focus');
          }
        });
      }
    }))
    .directive('ngEnter', () => ({
      link: function (scope, element, attrs) {
        element.bind('keydown keypress', event => {
          if (event.which === 13) {
            scope.$apply(() => {
              scope.$eval(attrs.ngEnter);
            });
            event.preventDefault();
          }
        });
      }
    }))
    .directive('capitalize', () => ({
      require: 'ngModel',

      link: function (scope, element, attrs, modelCtrl) {
        var capitalize = inputValue => {
          if (inputValue == undefined) {
            inputValue = '';
          }
          var capitalized = inputValue.toUpperCase();

          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }

          return capitalized;
        };

        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]);
      }
    }))
    /**
     * Dynamically renders the given template with the given context data
     */
    .directive('dynamicTemplate', ($compile, _) => ({
      scope: {
        'getTemplate': '&template',
        'getData': '&data'
      },

      link: function (scope, element) {
        var childScope = scope.$new(),
            template = null;

        function render() {
          element.html(template).show();
          $compile(element.contents())(childScope);
        }

        scope.$watch(scope.getTemplate, tpl => {
          template = tpl;
          render();
        });

        scope.$watch(scope.getData, data => {
          _.assign(childScope, data);
          render();
        }, true);
      }
    }))
    .directive('ngEnter', () => ({
      link: function (scope, element, attrs) {
        element.bind('keydown keypress', event => {
          if (event.which === 13) {
            scope.$apply(() => {
              scope.$eval(attrs.ngEnter);
            });
            event.preventDefault();
          }
        });
      }
    }));
