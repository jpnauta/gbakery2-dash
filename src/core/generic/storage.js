import { CoreExtMomentModule } from "../ext/moment";
import ngLocalStorage from 'angular-local-storage';

export const CoreGenericStorageModule = angular.module('core.generic.storage', [ngLocalStorage, CoreExtMomentModule.name])
/**
 * Wrapper class for storing data in local storage
 */
    .service('storage', localStorageService => ({
      /**
       * Stores a key-value pair
       * @param key the key to set
       * @param value the value to set
       */
      set: function (key, value) {
        return localStorageService.set(key, value);
      },

      /**
       * Retrieves a key-value pair
       * @param key the key to set
       */
      get: function (key) {
        return localStorageService.get(key) || undefined;
      },

      /**
       * Removes a key-value pair
       * @param key the key to set
       */
      remove: function (key) {

        return localStorageService.remove(key);
      },

      /**
       * Removes all key-value pairs. Optionally takes a regular expression string and removes matching.
       *
       * @param regex {String}
       */
      clearAll: function (regex) {
        return localStorageService.clearAll(regex);
      }
    }))
    /**
     * Similar to `storage` module, but allows the user to specify when a key expires
     */
    .service('cacheStorage', (storage, moment) => ({
      /**
       * Stores a key-value pair
       * @param key the key to set
       * @param value the value to set
       * @param expiresAt date when value expires at
       */
      set: function (key, value, expiresAt) {
        return storage.set(key, {
          $value: value,
          $expiresAt: expiresAt
        });
      },

      /**
       * Retrieves a key-value pair
       * @param key the key to set
       * @return {*} undefined if no value found, null if entry expired, or the value set
       */
      get: function (key) {
        var entry = storage.get(key);

        if (entry && typeof entry === 'object') {  // Cache hit
          if (!entry.$expiresAt || moment(entry.$expiresAt).toDate() >= moment().toDate()) {
            // Entry has not expired
            return entry.$value;
          } else {
            // Entry has expired
            storage.remove(key);
            return null;
          }
        }
        return undefined;
      },

      /**
       * Removes a key-value pair
       * @param key the key to set
       */
      remove: storage.remove,

      /**
       * Removes all key-value pairs. Optionally takes a regular expression string and removes matching.
       *
       * @param regex {String}
       */
      clearAll: storage.clearAll
    }))
    .provider('userCacheStorage', function () {
      var baseKey = '_usr';
      var sessionKey = null;
      var provider = this;

      /**
       * Sets the key which identifies the user currently logged in. Used to detect whether or not the
       * user has logged out.
       * @param {String} key the session key
       */
      function setSessionKey(key) {
        sessionKey = key;
      }

      function getKey(key) {
        if (!sessionKey) {
          throw Error('Session key must be set before using cache');
        }

        return baseKey + sessionKey + key;
      }

      provider.setSessionKey = setSessionKey;
      provider.$getKey = getKey;

      provider.$get = ['cacheStorage', cacheStorage => ({
        /**
         * Stores a user-based key-value pair
         * @param key the key to set
         * @param value the value to set
         * @param expiresAt date when value expires at
         */
        set: function (key, value, expiresAt) {
          return cacheStorage.set(provider.$getKey(key), value, expiresAt);
        },

        /**
         * Retrieves a user-based key-value pair
         * @param key the key to set
         */

        get: function (key) {
          return cacheStorage.get(provider.$getKey(key));
        },

        /**
         * Removes a user-based key-value pair
         * @param key the key to set
         */
        remove: function (key) {
          return cacheStorage.remove(provider.$getKey(key));
        },

        /**
         * Clears any old keys that do not match the specified session key
         */
        clearOldSessions: function () {
          // Find and remove all keys match base key but not matching session key
          var regex = new RegExp('^' + baseKey + '(?!' + sessionKey + ').*');
          return cacheStorage.clearAll(regex);
        }
      })];
    });