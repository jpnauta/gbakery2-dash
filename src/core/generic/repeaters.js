import { CoreExtLodashModule } from "../ext/lodash";

export const CoreGenericRepeatersModule = angular.module('core.generic.repeaters', [CoreExtLodashModule.name])
/**
 * Calls a certain function every x seconds. Useful for polling information.
 *
 * @param func {function} method to call
 * @param opts {object}
 * - time: How often to call the function (in milliseconds)
 */
    .service('Repeater', ($timeout, _) => {
      function Repeater(func, opts) {
        var $current, $next;

        opts = _.assign({
          time: 1000,
          timeFunc: function () {
            return opts.time;
          }
        }, opts);
        var self = this;
        self.$func = func;

        self.$repeatAction = () => {
          if (self.$running) {
            $current = self.$func();
            $next = $timeout(self.$repeatAction, opts.timeFunc());
          }
        };

        self.start = () => {
          if (!self.$running) {
            self.$running = true;
            self.$repeatAction();
          }
          return self;
        };

        self.cancel = () => {
          if ($next) {
            var next = $next;
            $next = null;
            $timeout.cancel(next);
          }
          self.$running = false;
        };

        self.forceCall = () => {
          self.cancel();
          self.$repeatAction();
          return self.getCurrent();
        };

        self.getCurrent = () => $current;
      }

      return Repeater;
    });
